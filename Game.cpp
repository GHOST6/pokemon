/*
Naomi Ephraim – nephrai1@jhu.edu 571-435-6593
Steven Dalvin – sdalvin1@jhu.edu 330-651-6324
Nick Blair – nblair@jhu.edu 845-238-1555
Daniel Benarroch – dbenarr1@jhu.edu 410-960-3319
600.120
Game.cpp
6/28/13
 */

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_mixer.h>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include "Trainer.h"
#include "Game.h"
#include "View.h"
#include "Model.h"
#include "GUI.h"
#include "Input.h"
#include "Music.h"

bool wait = true;

Game::Game() : done(false), cursor(0), t1(NULL), t2(NULL), endTurn(false), gui(NULL), input(NULL) { }

Game::Game(View * v, Controller * c, Trainer * trainer1, Trainer * trainer2) //constructor
{
  done = false;
  cursor = 0;
  gui = v;
  input = c;
  t1 = trainer1; //player 1
  t2 = trainer2; //player 2
  endTurn = false;
}

//destructor
Game::~Game() {}

Game & Game::operator=(const Game & g)
{
  done = g.done;
  cursor = g.cursor;
  gui = g.gui;
  input = g.input;
  t1 = g.t1;
  t2 = g.t2;
  endTurn = g.endTurn;
  return *this;
}

Pokemon * Game::getActivePokemon(Trainer * trainer)
{
  for (int i = 0; i < trainer->getNum_pokemon(); i++) { //find active pokemon
    if (trainer->getPokemon(i).getActive())
      return trainer->getPokemonPtr(i);
  }
  trainer->getPokemon(0).setActive(true); //else (this should never be reached)
  return trainer->getPokemonPtr(0);  
}

Trainer * Game::getCurrentTurn()
{
  if (t1->getIs_turn()) //checking whose turn
    return t1;
  return t2;
}

Trainer * Game::getOtherTrainer()
{
  if (t1->getIs_turn()) //finding whose turn
    return t2;
  return t1;
}

std::vector<Move> Game::getAbilities(Pokemon& pokemon)
{
  return pokemon.getMoves(); //return moves vector
}

int Game::getCursorPosition()  
{
  return cursor; //cursor position
}

void Game::drawMessage(std::string input)
{
  gui->displayTextBox();
  gui->DrawWrapText(input, (float) 20, (float) 310, 28); //call draw wrap text function
  gui->flip();
  SDL_Delay(2000); //delay
}

SDL_Surface * Game::loadImage(const char * filename)
{
  SDL_Surface * temp = IMG_Load(filename); //load an image
  if (temp)
    {
      SDL_Surface * optimized = SDL_DisplayFormat(temp); //set surface
      SDL_FreeSurface(temp); //free surface
      return optimized;
    }
  return 0;
}

void Game::switchTurn()
{
  endTurn = true; //flag to switch turns
  if (t1->getIs_turn()) { //switch
    t1->setIs_turn(false);
    t2->setIs_turn(true);
  }
  else { //switch
    t2->setIs_turn(false);
    t1->setIs_turn(true);
  }
}

int Game::gameLoop()
{
  if (SDL_Init(SDL_INIT_EVERYTHING) < 0)  { //initialize
    printf("Failed to initialize SDL\n");
    return 0;
  }
  atexit(SDL_Quit); //to do at exit
  if( TTF_Init() == -1 ) //initialize font
    return false;    

  Music m;
  m.startBattleMusic();

  SDL_WM_SetCaption("Pokemon", 0); //initialize window caption
  SDL_ShowCursor(SDL_DISABLE);
  SDL_EnableKeyRepeat(0, 0);
  gui->displayBackground(); //display white background
  gui->flip();
  if (rand() % 100 < 50) //randomly choose who goes first
    t1->setIs_turn(true);
  else 
    t2->setIs_turn(true);
  t1->setPokemonActive("pikachu", true);
  t2->setPokemonActive("pikachu", true);
  gui->displayPokemonMenu(); //display pokemon names
  gui->flip();
  cursor = 7; //set cursor
  while (cursor != 0) //before it returns to main menu
    doInput();
  gui->displayPokemonMenu(); //display menu for second player, repeat
  gui->flip();
  cursor = 7;
  while (cursor!= 0)  
    doInput();
  unsigned previous = SDL_GetTicks();  
  unsigned framerate = 1000/20;  
  while (!done) { //while game is not over
    endTurn = false;
    gui->updateHealth(); //display current state
    gui->flip();
    std::stringstream stream; 
    stream << getCurrentTurn()->getName(); //get current turn
    stream << "\'s turn!\n";
    std::string input = stream.str();
    drawMessage(input); //print whose turn it is
    unsigned now = SDL_GetTicks();
    unsigned delta = now - previous;
    previous = now;
    if (delta < framerate)
      SDL_Delay(framerate - delta);
    while(!endTurn && !done) //until the turn is over
      doInput();
  }
  m.stopBattleMusic();
  gui->freeVidScreen(); //free screen
  TTF_Quit(); //quit font
  SDL_Quit(); //quit sdl
  return true;
}

bool Game::beats(std::string a, std::string b)
{
  if ((a.compare("water") == 0) && (b.compare("fire") == 0))
    return true;
  else if ((a.compare("fire") == 0) && (b.compare("grass") == 0))
    return true;
  else if ((a.compare("grass") == 0) && (b.compare("electric") == 0))
    return true;
  else if ((a.compare("electric") == 0) && (b.compare("water") == 0))
    return true;
  return false;
}


std::string Game::use_move(Move m)
{
  Pokemon * a = getActivePokemon(getCurrentTurn()); //get active pokemon
  Pokemon * b = getActivePokemon(getOtherTrainer()); //get target pokemon
  std::stringstream stream;
  Music music;
  stream << a->getName() << " used " << m.getName() << " ";
  int miss = (rand() % 100);
  if (miss < 15 && m.getName() != "shell" && m.getName() != "solar regeneration" && m.getName() != "destruct") {
    if (m.getName() == "party" || m.getName() == "homework" || m.getName() == "flash")
      a->setNum_special(a->getNum_special() - 1);
    stream << ". It missed!\n";
    std::string output = stream.str();
    switchTurn();
    return output;
  }
  if (m.getName().compare("flash") == 0) { //if flash is being used
    if(a->getNum_special() <= 0) //if out of special moves
      return "Oops! Out of that move.\n";
    if ((a->setNum_special(a->getNum_special() - 1)) <= 0)
      a->removeMove("flash"); //remove move if out of special
    b->setAttack(b->getAttack() - 5);
    stream << " decreasing the attack of " << b->getName() << " to " << b->getAttack() << ".";
    music.flashfx();
    SDL_Delay(2000);
    music.freeFlashfx();
    std::string output = stream.str(); //convert to string
    switchTurn(); //switch turn
    return output; //return message string
  }
  else if (m.getName().compare("party") == 0) { //if party is being used
    if (a->getNum_special() <= 0) //if out of special moves
      return "Oops! out of that move.\n";
    if ((a->setNum_special(a->getNum_special() - 1)) <= 0)
      a->removeMove("party");
    int damage_done = a->getAttack() + m.getDamage() - b->getDefense(); //calculating damage done
    int crit = (rand() % 100); //if it's a critical hit
    if ((b->getName().compare("joanne") != 0) && ((b->getName().compare("james") == 0) || (crit < 10))) {
      damage_done += 7;
      stream << ", it was a critical hit!\n";
    }
    if (damage_done < 0)
      damage_done = 0;
    b->setHealth(b->getHealth() - damage_done);
    stream << b->getName() << " has a nasty hangover and is asleep.\n";
    music.partyfx(); //music
    SDL_Delay(2000);
    music.freePartyfx();
    if (b->getHealth() <= 0) { //checking if a pokemon has 0 health
      getCurrentTurn()->setExperience(getCurrentTurn()->getExperience() + 15);
      b->setHealth(0);
      std::string tempName = b->getName();
      stream << tempName << " fainted! \n";
      gui->updateHealth();
      getOtherTrainer()->removePokemon(b->getName());
      getOtherTrainer()->setNum_pokemon(getOtherTrainer()->getNum_pokemon() - 1);
      if (getOtherTrainer()->hasNoPokemon()) {
	getCurrentTurn()->setExperience(getCurrentTurn()->getExperience() + 50);
	stream << getOtherTrainer()->getName() << " has been defeated! \n";
	done = true;
      }
      else {
        drawMessage(stream.str());
        gui->flip();
        SDL_Delay(2000);
	stream.str(std::string());
	stream << " ";
	getOtherTrainer()->setActivePokemon(0, true);
	switchTurn();
	cursor = 18;
	wait = false;
	while (cursor != 0)
	  doInput();
	//switchTurn();
      }
    }
    std::string output = stream.str();
    return output;
  }
  else if (m.getName().compare("shell") == 0) { //if shell is being used
    if (a->getNum_special() <= 0) //if out of special moves
      return "Oops! Out of that move.\n";
    if ((a->setNum_special(a->getNum_special() - 1)) <= 0)
      a->removeMove("shell");
    a->setDefense(a->getDefense() + 5); //set defense
    stream << " increasing his defense to " << a->getDefense() << ".";
    music.shellfx();
    SDL_Delay(2000);
    music.freeShellfx();
    std::string output = stream.str();
    switchTurn();
    return output;
  }
  else if (m.getName().compare("solar regeneration") == 0) { //if solar regeneration is being used
    if (a->getNum_special() <= 0) //if out of moves
      return "Oops! Out of that move.\n"; 
    if ((a->setNum_special(a->getNum_special() - 1)) <= 0)
      a->removeMove("solar regeration");
    if (a->getHealth() > a->getTotalHealth() + 8)
      a->setHealth(a->getTotalHealth());
    else
      a->setHealth(a->getHealth() + 8); //set health
    stream << " to regain 8 health. \n";
    music.solarRegenerationfx();
    SDL_Delay(2000);
    music.freeSolarRegenerationfx();
    std::string output = stream.str();
    switchTurn();
    return output;
  }
  else if (m.getName().compare("homework") == 0) {
    if (a->getNum_special() <= 0)
      return "Oops! Out of that move.\n";
    if ((a->setNum_special(a->getNum_special() - 1)) <= 0) 
      a->removeMove("homework");
    int damage_done = a->getAttack() + m.getDamage() - b->getDefense();
    int crit = (rand() % 100); //if it's a critical hit
    if (crit < 18) {
      damage_done += 7;
      stream << ", it was a critical hit!\n";
    }
    if (damage_done < 0)
      damage_done = 0;
    b->setHealth(b->getHealth() - damage_done);
    stream << b->getName() << " is buried in a mountain of homework and is incapacitated for a turn.\n";
    music.homeworkfx();
    SDL_Delay(2000);
    music.freeHomeworkfx();
    if (b->getHealth() <= 0) { //checking if a pokemon has 0 health
      getCurrentTurn()->setExperience(getCurrentTurn()->getExperience() + 15);
      b->setHealth(0);
      std::string tempName = b->getName();
      stream << tempName << " fainted! \n";
      gui->updateHealth();
      getOtherTrainer()->removePokemon(b->getName());
      getOtherTrainer()->setNum_pokemon(getOtherTrainer()->getNum_pokemon() - 1);
      if (getOtherTrainer()->hasNoPokemon()) {
	getCurrentTurn()->setExperience(getCurrentTurn()->getExperience() + 50);
	stream << getOtherTrainer()->getName() << " has been defeated!\n";
	done = true;
      }
      else {
        drawMessage(stream.str());
        gui->flip();
        SDL_Delay(2000);
        stream.str(std::string());
	stream << " ";
        getOtherTrainer()->setActivePokemon(0, true);
        switchTurn();
	cursor = 18;
	wait = false;
        while (cursor != 0)
          doInput();
        //switchTurn();
      }
    }
    std::string output = stream.str();
    return output;
  }
  else if (m.getName().compare("destruct") == 0) {
    done = true;
    stream << " which caused a massive SEGFAULT killing everyone and ending the game. (Intentional)\n";
    music.destructfx();
    SDL_Delay(5000);
    music.freeDestructfx();
    std::string output = stream.str();
    return output;
  }
  else { //any non-special move
    int damage_done = a->getAttack() + m.getDamage() - b->getDefense();
    if (beats(m.getType(), b->getType())) { //determines who wins based on type
      damage_done += 20;
      stream << ", it was super effective!\n";
    }
    else if (beats(b->getType(), m.getType())) {
      damage_done -= 10;
      stream << ", it was not very effective!\n";
    }
    else {
      int crit = (rand() % 100); //if it's a critical hit
      if (crit < 18) {
	damage_done += 7;
	stream << ", it was a critical hit!\n";
      }
    }
    if (damage_done < 0)
      damage_done = 0;
    b->setHealth(b->getHealth() - damage_done);
    //displaying sounds for each move not specified above
    if (m.getName() == "tackle") {
      music.tacklefx();
      SDL_Delay(2000);
      music.freeTacklefx();
    }
    else if (m.getName() == "scratch") {
      music.scratchfx();
      SDL_Delay(2000);
      music.freeScratchfx();
    }
    else if (m.getName() == "ember") {
      music.emberfx();
      SDL_Delay(2000);
      music.freeEmberfx();
    }
    else if (m.getName() =="git") {
      music.gitfx();
      SDL_Delay(2000);
      music.freeGitfx();
    }
    else if (m.getName() =="java") {
      music.javafx();
      SDL_Delay(2000);
      music.freeJavafx();
    }
    else if (m.getName() =="leaf blade") {
      music.leafBladefx();
      SDL_Delay(2000);
      music.freeLeafBladefx();
    }
    else if (m.getName() =="shock") {
      music.shockfx();
      SDL_Delay(2000);
      music.freeShockfx();
    }
    else if (m.getName() =="water gun") {
      music.waterGunfx();
      SDL_Delay(2000);
      music.freeWaterGunfx();
    }
    if (b->getHealth() <= 0) { //checking if a pokemon has 0 health
      b->setHealth(0);
      std::string tempName = b->getName();
      stream << tempName << " fainted! \n";
      getCurrentTurn()->setExperience(getCurrentTurn()->getExperience() + 15);
      gui->updateHealth(); //update screen
      getOtherTrainer()->removePokemon(b->getName());
      getOtherTrainer()->setNum_pokemon(getOtherTrainer()->getNum_pokemon() - 1);
      if (getOtherTrainer()->hasNoPokemon()) {
	getCurrentTurn()->setExperience(getCurrentTurn()->getExperience() + 50);
	stream << getOtherTrainer()->getName() << " has been defeated!\n";
	done = true;
      }
      else { //if a pokemon fainted but the trainer has more pokemon
	drawMessage(stream.str());
	gui->flip();
	SDL_Delay(2000);
	stream.str(std::string());
	stream << " ";
	getOtherTrainer()->setActivePokemon(0, true);
	switchTurn();
	cursor = 18;
	wait = false;
	while (cursor != 0)
	  doInput();
	//switchTurn();
      } 
    }
    std::string output = stream.str(); //message
    switchTurn();
    return output;
  }
}

void Game::doInput()
{
  if (wait)
      cursor = input->handleInput(cursor);
  wait = true;
  switch(cursor) {
  case 0: //trainer screen, fight
    gui->displayMenu(); //display main menu
    gui->displayMenuCursor1(); //display cursor
    gui->flip();
    break;
  case 1: //trainer screen, item
    gui->displayMenuCursor2();
    gui->flip();
    break;
  case 2: //trainer screen, pkmn
    gui->displayMenuCursor3();
    gui->flip();
    break;
  case 3: //trainer screen, run
    gui->displayMenuCursor4();
    gui->flip();
    break;
  case 4: //fight screen basic
    gui->displayFightCursor1();
    gui->flip();
    break;
  case 5: //fight screen elemental
    gui->displayFightCursor2();
    gui->flip();
    break;
  case 6: //fight screen special
    if (getActivePokemon(getCurrentTurn())->getNum_special() <= 0) {
      cursor = 4;
      break;
    }
    gui->displayFightCursor3();
    gui->flip();
    break;
  case 7: //pkmn screen 1
    gui->displayPkmnCursor1();
    gui->flip();
    break;
  case 8: //pkmn screen 2
    if (getCurrentTurn()->getNum_pokemon() <= 1) {
      cursor = 7;
      break;
    }
    gui->displayPkmnCursor2();
    gui->flip();
    break;
  case 9: //pkmn screen 3
    if (getCurrentTurn()->getNum_pokemon() <= 2) {
      cursor = 7;
      break;
    }
    gui->displayPkmnCursor3();
    gui->flip();
    break;
  case 10: //pkmn screen 4
    if (getCurrentTurn()->getNum_pokemon() <= 3) {
      cursor = 7;
      break;
    }
    gui->displayPkmnCursor4();
    gui->flip();
    break;
  case 11: //pkmn screen 5
    if (getCurrentTurn()->getNum_pokemon() <= 4) {
      cursor = 7;
      break;
    }
    gui->displayPkmnCursor5();
    gui->flip();
    break;
  case 12: //pkmn screen 6
    if (getCurrentTurn()->getNum_pokemon() <= 5) {
      cursor = 7;
      break;
    }
    gui->displayPkmnCursor6();
    gui->flip();
    break;
    //item cursors
  case 17: //pressing enter on trainer screen, moves
    gui->displayMoves(); //display moves menu
    gui->displayFightCursor1(); //display cursor
    gui->flip();
    cursor = 4;
    break;
  case 18: //pressing enter on trainer screen, pkmn
    gui->displayPokemonMenu(); //display pokemon menu
    gui->displayPkmnCursor1(); //display cursor
    gui->flip();
    cursor = 7;
    break;
  case 19: //pressing enter on trainer screen, item
    {
    cursor = 0;
    gui->displayTextBox();
    std::string input = "Items at CharMar were too expensive!\n";
    drawMessage(input);
    gui->flip();
    //display cursor
    break;
    }
  case 20: //pressing enter on move screen, basic
    {
      cursor = 0; //reset cursor
      std::string input = use_move(getActivePokemon(getCurrentTurn())->getMove(0)); //do move
      drawMessage(input); //output message
      if (done)
	break;
      gui->updateHealth(); //display current health
      gui->flip();
      break;
    }
  case 21: //pressing enter on move screen, elemental
    {
      cursor = 0;
      std::string input = use_move(getActivePokemon(getCurrentTurn())->getMove(1));
      drawMessage(input);
      if (done)
	break;
      gui->updateHealth();
      gui->flip();
      break;
    }
  case 22: //pressing enter on move screen, special
    {
      if (getActivePokemon(getCurrentTurn())->getNum_special() <= 0) {
	cursor = 4;
	break;
      }
      cursor = 0;
      std::string input = use_move(getActivePokemon(getCurrentTurn())->getMove(2));
      drawMessage(input);
      SDL_Delay(2000);
      if (done)
	break;
      gui->updateHealth();
      gui->flip();
      break;
    }
  case 23: case 24: case 25: case 26: case 27: case 28: //pressing enter on pkmn screen, pokemon 1-6
    {
      int which = cursor - 23;
      cursor = 0; //reset cursor
      int numPokes = getCurrentTurn()->getNum_pokemon();
      int saveActive = 0;
      for (int i = 0; i < numPokes; i++) {
	if (getCurrentTurn()->getPokemon(i).getActive()) {
	  saveActive = i;
	  getActivePokemon(getCurrentTurn())->setActive(false); //changing active pokemon
	}
      }
      getCurrentTurn()->setPokemonActive(getCurrentTurn()->getPokemon(which).getName(), true); //setting active pokemon
      gui->displayTextBox(); //display pokemon
      gui->flip();
      std::stringstream stream;
      stream << getActivePokemon(getCurrentTurn())->getName();
      stream << " is ready to fight!\n";
      std::string input = stream.str();
      drawMessage(input); //message
      if (saveActive == which)
        switchTurn();
      switchTurn();
      break;
    }
  case 29: //pressing enter on run, or escape in trainer screen
    cursor = 0;
    done = true;
    break;
  default: //pressing anything else
    break;
  }
}

int main(int argc, char * argv[])
{
  if (argc != 3) { //player names
    std::cout << "Usage: Player names\n";
    return 0;
  }

  std::string name1(argv[1]);
  std::string name2(argv[2]);

  Trainer t1(name1); //creating trainer 1
  Trainer t2(name2); //creating trainer 2

  View * gui = new GUI(&t1, &t2);
  Controller * input = new Input();
  Model * game = new Game(gui, input, &t1, &t2);

  //start screen
  gui->startScreen();
  gui->flip();
  SDL_Delay(7000);

  game->gameLoop(); //play

  return 0;
}
