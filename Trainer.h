/*
Naomi Ephraim – nephrai1@jhu.edu 571-435-6593
Steven Dalvin – sdalvin1@jhu.edu 330-651-6324
Nick Blair – nblair@jhu.edu 845-238-1555
Daniel Benarroch – dbenarr1@jhu.edu 410-960-3319
600.120
Trainer.h
6/28/13
 */

#ifndef _TRAINER_H
#define _TRAINER_H

#include <string>
#include "Pokemon.h"

class Trainer
{
 protected:
  std::string name;
  int experience;
  int num_pokemon;
  std::vector<Pokemon> pokemons;
  bool is_turn;

 public:
  Trainer(); //constructor
  Trainer(std::string);	//constructor from name
  Trainer & operator=(const Trainer&); //equals operator
  friend std::ostream & operator<<(std::ostream &, const Trainer &);
  Trainer(const Trainer &); //copy constructor
  
  ~Trainer();
  
  //accessors
  /** Returns a trainer's name. 
   */
  std::string getName() const { return name; }

  /** Returns a trainer's experience. 
   */
  int getExperience() const { return experience; }

  /** Returns a trainer's number of pokemon. 
   */
  int getNum_pokemon() const { return num_pokemon; }

  /** Returns the trainer's is_turn flag. 
   */
  bool getIs_turn() const { return is_turn; }

  /** Returns a trainer's pokemon. 
   */
  std::vector<Pokemon> getPokemons() const { return pokemons; }

  /** Returns a pointer to a trainer's pokemon. 
   */
  std::vector<Pokemon> * getPokemonsPtr() { return &pokemons; }

  /** Returns the trainer's pokemon at index i. 
   */
  Pokemon getPokemon(int i) const { return pokemons[i]; }

  /** Returns a pointer to the trainer's pokemon at index i. 
   */
  Pokemon * getPokemonPtr(int i) {return &(pokemons[i]); }

  /** Returns whether a trainer has pokemon. 
   */ 
  bool hasNoPokemon() const { return !pokemons.size(); }
  
  //mutators
  /** Set a trainer's is_turn flag. 
   */
  void setIs_turn(bool a) { is_turn = a; }

  /** Set a trainer's number of pokemon. 
   */ 
  void setNum_pokemon(int i) { num_pokemon = i; }

  /** Set a trainer's active pokemon to a pokemon based on an index.  
   */
  void setActivePokemon(int index, bool a) { pokemons[index].setActive(a); }

  /** Remove a pokemon from a trainer. 
   */
  void removePokemon(std::string);

  /** Sets a pokemon's health. 
   */
  void setPokemonHealth(std::string, int);

  /** Sets a pokemon's attack strength. 
   */
  void setPokemonAttack(std::string, int);

  /** Sets a pokemon's defense strength. 
   */
  void setPokemonDefense(std::string, int);

  /** Sets a trainer's pokemon's active flag to the given boolean. 
   */
  void setPokemonActive(std::string, bool); 

  /** Sets a trainer's experience. 
   */
  void setExperience(int a) { experience = a; }
};

#endif
