#include "Bulbasaur.h"
Bulbasaur::Bulbasaur(): Pokemon(), name("bulbasaur"), type("grass")
{
	health = 85;
	moves = new Move[3];
	moves[0] = Tackle();
	moves[1] = LeafBlade();
	moves[2] = SolarRegeneration();
}

Bulbasaur::~Bulbasaur()
{
	delete []moves;
}
