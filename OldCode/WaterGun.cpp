class WaterGun : Move
{

	public:
	WaterGun(): name("water gun"), damage(25), type("water"), cause_sleep(false), message("used water guns to blast") {}

	~WaterGun() {}

};
