class Scratch : Move
{

	public:
	Scratch(): Move(), name("scratch"), damage(10), type("none"), cause_sleep(false), message("used scratch on") {}

	~Scratch() {}
};
