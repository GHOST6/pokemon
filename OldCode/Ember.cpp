class Ember : Move
{

	public:
	Ember(): name("ember"), damage(25), type("fire"), cause_sleep(false), message("used ember to burn") {}

	~Ember() {}

};
