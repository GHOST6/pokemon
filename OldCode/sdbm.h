#ifndef _SDBM_H
#define _SDBM_H

#include "Trainer.h"

/**
 * Create a file called database.db.
 */
void sdbm_create();

/**
 * Open a file called database.db, and load its contents into memory. 
 */
void sdbm_open();

/**
 * Synchronize all changes in database (if any) to disk.
 * Useful if implementation caches intermediate results
 * in memory instead of writing them to disk directly.
 * Return true on success, false on failure.
 */
void sdbm_sync();

/**
 * Close database, synchronizing changes (if any). Return
 * true on success, false on failure.
 */
void sdbm_close();

/*
 * Is given trainer in database?
 */
bool sdbm_has(Trainer);

/**
 * Insert given key and value into database as a new
 * association. Return true on success, false on
 * failure.
 *
 * Precondition: !sdbm_has(key)
 */
void sdbm_insert(const Trainer *);

#endif /* _SDBM_H */
