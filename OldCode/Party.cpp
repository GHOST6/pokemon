class Party : Move
{

	public:
	Party(): name("party"), damage(0), type("none"), cause_sleep(true), message("threw an awesome party, giving a hangover to") {}

	~Party() {}

	int use_move(Pokemon a, Pokemon b)
	{
		if (a.num_special <= 0)
			return 0;
		if ((--a.num_special) <= 0)
			delete a.moves[2];
		b.asleep = true;
		return 0;
	}

};
