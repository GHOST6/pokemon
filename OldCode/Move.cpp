#include "Move.h"

Move::Move(): name(""), type(""), damage(0), cause_sleep(false), message(""){}
Move::~Move(){}
std::string Move::getName() const { return this->name; }
std::string Move::getType() const { return this->type; }
int Move::getDamage() const { return this->damage; }
bool Move::getCause_sleep() const { return this->cause_sleep; }
std::string Move::getMessage() const {return this->message; }
/*
Ember::Ember()
{
	name = "ember";
	damage = 25;
	type = "fire";
	cause_sleep = false;
	message = "used ember to burn";
}
Ember::~Ember(){}


Flash::Flash()
{
	name = "flash";
	damage = 0;
	type = "none";
	cause_sleep = false;
	message = "flashed, decreasing the attack of";
}
Flash::~Flash(){}


LeafBlade::LeafBlade()
{
	name = "leaf blade";
	damage = 25;
	type = "grass";
	cause_sleep = false;
	message = "used his razor sharp leaves to cut";
}
LeafBlade::~LeafBlade(){}


Party::Party()
{
	name = "party";
	damage = 0;
	type = "none";
	cause_sleep = true;
	message = "threw an awesome party, giving a hangover to";
}
Party::~Party(){}


Scratch::Scratch()
{
	name = "scratch";
	damage = 10;
	type = "none";
	cause_sleep = false;
	message = "used scratch on";
}
Scratch::~Scratch(){}


Shell::Shell()
{
	name = "shell";
	damage = 0;
	type = "none";
	cause_sleep = false;
	message = "increased his defense by hiding in his shell from";
}
Shell::~Shell(){}


Shock::Shock()
{
	name = "shock";
	damage = 25;
	type = "electric";
	cause_sleep = false;
	message = "shocked";
}
Shock::~Shock(){}


SolarRegeneration::SolarRegeneration()
{
	name = "solar regeneration";
	damage = 0;
	type = "none";
	cause_sleep = false;
	message = "regenerated some health and didn't attack";
}


Tackle::Tackle()
{
	name = "tackle";
	damage = 10;
	type = "none";
	cause_sleep = false;
	message = "used tackle on";
}
Tackle::~Tackle(){}


WaterGun::WaterGun()
{
	name = "water gun";
	damage = 25;
	type = "water";
	cause_sleep = false;
	message = "used water guns to blast";
}
WaterGun::~WaterGun(){}

#include <iostream>
int main(void)
{
	Move move;
	move = LeafBlade();
	std::cout << move.getName() << std::endl;
}
*/
