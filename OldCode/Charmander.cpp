#include "Charmander.h"

Charmander::Charmander(): Pokemon(), name("charmander"), type("fire")
{
	attack = 60;
	defense = 40;
	moves = new Move[3];
	moves[0] = Tackle();
	moves[1] = Ember();
	moves[2] = Flash();
}

Charmander::~Charmander()
{
	delete []moves;
}
