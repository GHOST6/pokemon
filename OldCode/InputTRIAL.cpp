#include <iostream>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include "Controller.h"
class Input : public Controller
{

public:

  int HandleInput(int cursor)
  {
    SDL_Event event;
    while(SDL_PollEvent(&event)){
      switch (event.type) {
      case SDL_QUIT:
	cursor = 29;
	break;
      case SDL_KEYDOWN:
	if (cursor >= 0 && cursor <= 3) {
	  switch (event.key.keysym.sym) {
	  case SDLK_UP: case SDLK_DOWN:
	    if (cursor == 0 || cursor == 1)
	      cursor += 2;
	    if (cursor == 2 || cursor == 3)
	      cursor -= 2;
	    break;
	  case SDLK_LEFT: case SDLK_RIGHT:
	    if (cursor == 0 || cursor == 2)
	      cursor += 1;
	    if (cursor == 1 || cursor == 3)
	      cursor -= 1;
	    break;
	  case SDLK_RETURN:
	    switch (cursor) {
	    case 0: 
	      cursor = 17; //change menus, attrainerscreen = false, fightbox = true
	      break;
	    case 1:
	      cursor = 18; //change menus, pkmnbox
	      break;
	    case 2:
	      cursor = 19; //change menus, item
	      break;
	    case 3:
	      cursor = 29;
	      break;
	    default:
	      break;
	    } //close switch cursor
	  case SDLK_ESCAPE:
	    cursor = 29;
	    break;
	  default:
	    break;
	  } //close switch keysym
	} //close attrainerscreen
	if (cursor >=4 && cursor <= 6) { //fightBox
          switch (event.key.keysym.sym) {
	  case SDLK_DOWN:
	    cursor = (cursor - 4 + 1) % 3 + 4;
	    break;
	  case SDLK_UP:
	    cursor = (cursor - 4 - 1) % 3 + 4;
	    break;
	  case SDLK_RETURN:
	    cursor += 16;
	    break;
	  case SDLK_ESCAPE:
	    cursor = 0;
            break;
	  default:
	    break;
	  }
	}
	if (cursor >= 7 && cursor <=11) { //pkmnbox
          switch (event.key.keysym.sym) {
          case SDLK_DOWN:
            cursor = (cursor - 7 + 1) % 4 + 7;
            break;
          case SDLK_UP:
            cursor = (cursor - 7 - 1) % 4 + 7;
            break;
          case SDLK_RETURN:
	    cursor += 16;
            break;
          case SDLK_ESCAPE:
	    cursor = 0;
            break;
          default:
            break;
          }
	}
	if (cursor >= 13 && cursor <= 16) {
	  //fix later 
	}
      }
	}
    return cursor;
  }
};

int main()
{
	int c = 2;
	//Input mytest;
	//int c = mytest.HandleInput(cursor);
	if (SDL_Init (SDL_INIT_VIDEO) < 0){
		std::cout << "Could not initialize: " << SDL_GetError() << std::endl;
		exit(-1);
	}
	SDL_Event event;
	SDL_Surface * screen;	
	if ((screen = SDL_SetVideoMode(480, 432, 32, SDL_HWSURFACE)) == 0){
		std::cout << "Cannot set video mode" << std::endl;}
	SDL_EnableUNICODE(1);
	int x;
	while(SDL_PollEvent(&event)){
	  x = SDL_WaitEvent(&event);
	  std::cout<< "This actually ran" << std::endl;
		switch(event.type){
			case SDL_KEYDOWN:
				if(event.key.keysym.sym == SDLK_UP)
				  {
				    c++; break;
				  }
				if(event.key.keysym.sym == SDLK_DOWN)
				  {
				  c--; break;
				  }
		default:
		  std::cout << "it just went to default" << std::endl;
				break;
	}
	}
	std::cout << c << " " << x << " " << SDLK_DOWN << " " << event.key.keysym.sym << std::endl;
	return 0;
}
