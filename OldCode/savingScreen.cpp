#include <iostream>
#include <sstream>
#include <cstdlib>
#include "Screen.h"
#include "view.h"
#include "GUI.h"
#include "Trainer.h"
#include "Pokemon.h"
#include <vector>

/*
Screen::Screen()
{}
*/

Screen::Screen(Trainer & p1, Trainer & p2)
{
  player1 = p1;
  player2 = p2;
}
    
    //destructor
Screen::~Screen() //destructor
{}

//Accessors
void Screen::displayBackground() //displays a white background with an empty box
{
  myGUI.DrawRect((float) 0, (float) 0, (float) 480, (float) 432, (int) 0xFFFFFF);

  SDL_Surface * myIm = IMG_Load("TextBar.png"); 
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 0, (float) 291);
  SDL_FreeSurface(optimized);
}

void Screen::displayTextBox()
{
  SDL_Surface * myIm = IMG_Load("TextBar.png"); 
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 0, (float) 291);
  SDL_FreeSurface(optimized);
}

void Screen::displayMenu() //displays the menu
{
  Screen::displayBackground();
  SDL_Surface * myIm = IMG_Load("BottomBar.png"); //displays an image of the menu with options
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 0, (float) 291);
  SDL_FreeSurface(optimized);

  std::string opt1 = "Fight";
  std::string opt2 = "Item";
  std::string opt3 = "PkMn";
  std::string opt4 = "Run";

  myGUI.DrawText(opt1, (float) 250, (float) 330); //****** needs to be adjusted, as well as the ones below
  myGUI.DrawText(opt2, (float) 250, (float) 380); 
  myGUI.DrawText(opt3, (float) 380, (float) 330);
  myGUI.DrawText(opt4, (float) 380, (float) 380);
}

void Screen::displayMoves() //displays moves
{
  SDL_Surface * myIm = IMG_Load("BottomBar.png"); //display the box for moves
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 0, (float) 290); //*** eh- might not- THESE COORDS NEED FIXING
  SDL_FreeSurface(optimized); 

  // get trainer who's turn it is
  Trainer moveDisp;
  // player1.setActivePokemon(0, true); // *** DON"T KNOW HOW TO FIX THIS!!!!!
  if(player1.getIs_turn())
    {
      std::cout << "this should display" << std::endl;
      std::vector<Pokemon> test = player1.getPokemons();
      std::cout << "active status p1: " << test[0].getActive() << std::endl;
      moveDisp = player1; //******** Trainer must have it's copy constructor defined!!!!
      std::vector<Pokemon> test1 = moveDisp.getPokemons();
      std::cout << "active status after: " << test1[0].getActive() << std::endl;
    }
  else if(player2.getIs_turn())
    {
      moveDisp = player2;
    }
  std::vector<Pokemon> activep;
  int num_pokemon = 4; // This could be changed or could be a global variable!!!!!!!!!!!!!!!!!
  // look for the active pokemon
      std::vector<Pokemon> pokemons = moveDisp.getPokemons();
      //  moveDisp.setActivePokemon(0, true); //****** DONT KNOW HOW TO FIX!!!
  for(int i=0; i < num_pokemon; i++)
    {
      std::cout << "active? : " << pokemons[i].getActive() << std::endl;
      if((pokemons[i]).getActive())
	{
	  std::cout << "name goes here: " << pokemons[i].getName() << std::endl;
	activep.push_back(pokemons[i]);
	break;
	}
    }

  std::vector<Move> moves = activep[0].getMoves();
  std::string move1 = moves[0].getName();
  std::string move2 = moves[1].getName();
  std::string move3 = moves[2].getName();
  std::string move4 = "--";
  //check if there is a move 4
  /*
  if(moves[3] != NULL)
    {
      move4 = moves[3].getName();
    }
  else 
  */


  // myGUI.DrawRect((float) 125, (float) 300, (float) 320, (float) 100, 0xFFFFFF);
  //draw the moves to the screen
  myGUI.DrawText(move1, (float) 240, (float) 315); //*********** NEED TO FIX X and Y COORDS!!!!!!!
  myGUI.DrawText(move2, (float) 240, (float) 340);
  myGUI.DrawText(move3, (float) 240, (float) 365);
  myGUI.DrawText(move4, (float) 240, (float) 390);
}

void Screen::displayItems() //displays items menu
{
  SDL_Surface * myIm = IMG_Load("*X*X*X*X*X*X*X*X*X*X*X*X"); //display the box for items
  if(myIm == NULL)
    {
      std::cerr << "Couldn't load item's image screen" << std::endl;
      exit(1);
    }
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 0, (float) 0); //** THESE COORDS NEED FIXING
  SDL_FreeSurface(optimized);
}

void Screen::displayPokemon() //displays the active pokemon
{

  // look for the active pokemon
  int num_pokemon = 4; //****** COULD BE GLOBAL
  std::vector<Pokemon> activep;
      std::vector<Pokemon> pokemons = player1.getPokemons();

  for(int i=0; i < num_pokemon; i++)
    {
      if((pokemons[i]).getActive())
	{
	activep.push_back(pokemons[i]);
	break;
	}
    }

  // look for the active pokemon
  std::vector<Pokemon> activep2;
     std::vector<Pokemon> pokemons2 = player2.getPokemons();
  for(int i=0; i < num_pokemon; i++)
    {
      if((pokemons2[i]).getActive())
	{
	  activep2.push_back(pokemons2[i]);
	break;
	}
    }



  SDL_Surface * myIm = IMG_Load((activep[0]).getImage_back().c_str()); //*******get image from pokemon class
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 20, (float) 180); //****** needs to be adjusted 
  SDL_FreeSurface(optimized);

  //player 2's pokemon ****************************** get image from pokemon class
  SDL_Surface * myIm1 = IMG_Load((activep2[0]).getImage_front().c_str()); //*******get image from pokemon class?
  SDL_Surface * optimized1 = SDL_DisplayFormat(myIm1);
  SDL_FreeSurface(myIm1);
  myGUI.DrawImage(optimized1, (float) 345, (float) 40); //****** needs to be adjusted 
  SDL_FreeSurface(optimized1);
}


void Screen::displayXP() //displays the trainer's experience
{

  std::string xp1 = "XP:";
  std::stringstream sa;
  sa << player1.getExperience();
  std::string xp2 = sa.str();
  std::string t1xp = xp1 + xp2;
  std::stringstream sa1;
  sa1 << player2.getExperience();
  std::string xp3 = sa1.str();
  std::string t2xp = xp1 + xp3;

  myGUI.DrawRect((float) 95, (float) 40, (float) 80, (float) 25, 0xFFFFFF);
  myGUI.DrawRect((float) 320, (float) 200, (float) 80, (float) 25, 0xFFFFFF);

  myGUI.DrawText(t1xp, (float) 320, (float) 205); //********* these coords need fixing
  myGUI.DrawText(t2xp, (float) 110, (float) 50);
}

void Screen::displayPokemonNames()
{

  std::vector<Pokemon> p1Active;
  std::vector<Pokemon> p2Active;
  int num_pokemon = 4; // This could be changed or could be a global variable!!!!!!!!!!!!!!!!!

  // look for the active pokemon
  std::vector<Pokemon> pokemons = player1.getPokemons();
  for(int i=0; i < num_pokemon; i++)
    {
      if((pokemons[i]).getActive())
	{
	  p1Active.push_back(pokemons[i]);
	break;
	}
    }

  pokemons = player2.getPokemons();
  for(int i=0; i < num_pokemon; i++)
    {
      if((pokemons[i]).getActive())
	{
	p2Active.push_back(pokemons[i]);
	break;
	}
    }

  std::string name1 = p1Active[0].getName(); //***** I think there needs to be a getName function and that this won't work!
  std::string name2 = p2Active[0].getName(); //***** not sure about the dereferencing and whatever

  //draw the names to the screen
  myGUI.DrawText(name1, (float) 300, (float) 180); //*********** NEED TO FIX X and Y COORDS!!!!!!!
  myGUI.DrawText(name2, (float) 100, (float) 25);

}



void Screen::displayPokemonMenu()
{
  Trainer disp;
  //draw rectangles for the pokemon
  myGUI.DrawRect((float) 0, (float) 0, (float) 480, (float) 72, (int) 0x0000FF);
  myGUI.DrawRect((float) 0, (float) 72, (float) 480, (float) 72, (int) 0xFF0000);
  myGUI.DrawRect((float) 0, (float) 144, (float) 480, (float) 72, (int) 0x00FF00);
  myGUI.DrawRect((float) 0, (float) 216, (float) 480, (float) 72, (int) 0xFFFF00);
  myGUI.DrawRect((float) 0, (float) 288, (float) 480, (float) 72, (int) 0x000000);
  myGUI.DrawRect((float) 0, (float) 360, (float) 480, (float) 72, (int) 0x000000);


  if(player1.getIs_turn()) //*********I think this might need to be a get_turn function or something too because it's private
    {
      disp = player1; //***** need = operator defined for trainer (copy constructor)
    }
  if(player2.getIs_turn()) //****** same deal
    {
      disp = player2;
    }

  int num_pokemon = 4; //***** COULD BE A GLOBAL VARIABLE AS WELL
  std::vector<Pokemon> pokemons = disp.getPokemons();
  if(num_pokemon > 0)
    {
      SDL_Surface * myIm = IMG_Load(pokemons[0].getImage_menu().c_str()); //**** should be some image associated with each pokemon
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 15, (float) 0); //****** needs to be adjusted 
  SDL_FreeSurface(optimized);

  std::string n1 = ((pokemons[0]).getName()); //**** I think this should be a get_name() function too!!!
  myGUI.DrawText(n1, (float) 150, (float) 0); //**** This needs to be adjusted!!!!!

  int hi1 = ((pokemons[0]).getHealth()); // **** I think it should be getHealth()
  int ht1 = ((pokemons[0]).getTotalHealth());
  std::stringstream sa1;
  sa1 << hi1 << "/" << ht1;
  std::string h1 = sa1.str(); // ***** Not sure that this works either
  myGUI.DrawText(h1, (float) 320, (float) 0); //**** Needs to be adjusted
    }


  if(num_pokemon > 1)
    {
      SDL_Surface * myIm2 = IMG_Load(pokemons[1].getImage_menu().c_str());
  SDL_Surface * optimized2 = SDL_DisplayFormat(myIm2);
  SDL_FreeSurface(myIm2);
  myGUI.DrawImage(optimized2, (float) 15, (float) 72); //****** needs to be adjusted 
  SDL_FreeSurface(optimized2);

  std::string n2 = ((pokemons[1]).getName()); //**** I think this should be a get_name() function too!!!
  myGUI.DrawText(n2, (float) 150, (float) 72); //**** This needs to be adjusted!!!!!

  int hi2 = ((pokemons[1]).getHealth()); // **** I think it should be getHealth()
  int ht2 = ((pokemons[1]).getTotalHealth());
  std::stringstream sa2;
  sa2 << hi2 << "/" << ht2;
  std::string h2 = sa2.str(); // ***** Not sure that this works either
  myGUI.DrawText(h2, (float) 320, (float) 72); //**** Needs to be adjusted
    }


  if(num_pokemon > 2)
    {
      SDL_Surface * myIm3 = IMG_Load(pokemons[2].getImage_menu().c_str());
  SDL_Surface * optimized3 = SDL_DisplayFormat(myIm3);
  SDL_FreeSurface(myIm3);
  myGUI.DrawImage(optimized3, (float) 15, (float) 144); //****** needs to be adjusted 
  SDL_FreeSurface(optimized3);

  std::string n3 = ((pokemons[2]).getName()); //**** I think this should be a get_name() function too!!!
  myGUI.DrawText(n3, (float) 150, (float) 144); //**** This needs to be adjusted!!!!!

  int hi3 = ((pokemons[2]).getHealth()); // **** I think it should be getHealth()
  int ht3 = ((pokemons[2]).getTotalHealth());
  std::stringstream sa3;
  sa3 << hi3 << "/" << ht3;
  std::string h3 = sa3.str(); // ***** Not sure that this works either
  myGUI.DrawText(h3, (float) 320, (float) 144); //**** Needs to be adjusted
    }


  if(num_pokemon > 3)
    {
      SDL_Surface * myIm4 = IMG_Load(pokemons[3].getImage_menu().c_str());
  SDL_Surface * optimized4 = SDL_DisplayFormat(myIm4);
  SDL_FreeSurface(myIm4);
  myGUI.DrawImage(optimized4, (float) 15, (float) 216); //****** needs to be adjusted 
  SDL_FreeSurface(optimized4);

  std::string n4 = ((pokemons[3]).getName()); //**** I think this should be a get_name() function too!!!
  myGUI.DrawText(n4, (float) 150, (float) 216); //**** This needs to be adjusted!!!!!

  int hi4 = ((pokemons[3]).getHealth()); // **** I think it should be getHealth()
  int ht4 = ((pokemons[3]).getTotalHealth());
  std::stringstream sa4;
  sa4 << hi4 << "/" << ht4;
  std::string h4 = sa4.str(); // ***** Not sure that this works either
  myGUI.DrawText(h4, (float) 320, (float) 216); //**** Needs to be adjusted
    }


  if(num_pokemon > 4)
    {
      SDL_Surface * myIm5 = IMG_Load(pokemons[4].getImage_menu().c_str());
  SDL_Surface * optimized5 = SDL_DisplayFormat(myIm5);
  SDL_FreeSurface(myIm5);
  myGUI.DrawImage(optimized5, (float) 15, (float) 288); //****** needs to be adjusted 
  SDL_FreeSurface(optimized5);

  std::string n5 = ((pokemons[4]).getName()); //**** I think this should be a get_name() function too!!!
  myGUI.DrawText(n5, (float) 150, (float) 288); //**** This needs to be adjusted!!!!!

  int hi5 = ((pokemons[4]).getHealth()); // **** I think it should be getHealth()
  int ht5 = ((pokemons[4]).getTotalHealth());
  std::stringstream sa5;
  sa5 << hi5 << "/" << ht5;
  std::string h5 = sa5.str(); // ***** Not sure that this works either
  myGUI.DrawText(h5, (float) 320, (float) 288); //**** Needs to be adjusted
    }


  if(num_pokemon > 5)
    {
      SDL_Surface * myIm6 = IMG_Load(pokemons[5].getImage_menu().c_str());
  SDL_Surface * optimized6 = SDL_DisplayFormat(myIm6);
  SDL_FreeSurface(myIm6);
  myGUI.DrawImage(optimized6, (float) 15, (float) 360); //****** needs to be adjusted 
  SDL_FreeSurface(optimized6);

  std::string n6 = ((pokemons[5]).getName()); //**** I think this should be a get_name() function too!!!
  myGUI.DrawText(n6, (float) 150, (float) 360); //**** This needs to be adjusted!!!!!

  int hi6 = ((pokemons[5]).getHealth()); // **** I think it should be getHealth()
  int ht6 = ((pokemons[5]).getTotalHealth());
  std::stringstream sa6;
  sa6 << hi6 << "/" << ht6;
  std::string h6 = sa6.str(); // ***** Not sure that this works either
  myGUI.DrawText(h6, (float) 320, (float) 360); //**** Needs to be adjusted
    }
}



void Screen::displayMenuCursor1() //menu 1
{
  //Screen::updateHealth();
  SDL_Surface * myIm = IMG_Load("Cursor.png");
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 230, (float) 325); //****** needs to be adjusted 
  SDL_FreeSurface(optimized);
}
void Screen::displayMenuCursor2() //menu 2
{
  //Screen::updateHealth();
  SDL_Surface * myIm = IMG_Load("Cursor.png");
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 230, (float) 375); //****** needs to be adjusted 
  SDL_FreeSurface(optimized);
}

void Screen::displayMenuCursor3() //menu 3
{
  //Screen::updateHealth();
  SDL_Surface * myIm = IMG_Load("Cursor.png");
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 360, (float) 325); //****** needs to be adjusted 
  SDL_FreeSurface(optimized);
}

void Screen::displayMenuCursor4() //menu 4
{
  //Screen::updateHealth();
  SDL_Surface * myIm = IMG_Load("Cursor.png");
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 360, (float) 375); //****** needs to be adjusted 
  SDL_FreeSurface(optimized);
}

void Screen::displayFightCursor1() //move 1
{
  Screen::displayMoves();
  SDL_Surface * myIm = IMG_Load("Cursor.png");
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 215, (float) 312); //****** needs to be adjusted 
  SDL_FreeSurface(optimized);
}

void Screen::displayFightCursor2() //move 2
{
  Screen::displayMoves();
  SDL_Surface * myIm = IMG_Load("Cursor.png");
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 215, (float) 337); //****** needs to be adjusted 
  SDL_FreeSurface(optimized);
}

void Screen::displayFightCursor3() //move 3
{
  Screen::displayMoves();
  SDL_Surface * myIm = IMG_Load("Cursor.png");
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 215, (float) 362); //****** needs to be adjusted 
  SDL_FreeSurface(optimized);
}

void Screen::displayFightCursor4() //move 4
{
  Screen::displayMoves();
  SDL_Surface * myIm = IMG_Load("Cursor.png");
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 215, (float) 387); //****** needs to be adjusted 
  SDL_FreeSurface(optimized);
}

void Screen::displayPkmnCursor1() //pokemon 1
{
  Screen::displayPokemonMenu();
  SDL_Surface * myIm = IMG_Load("Cursor.png");
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 0, (float) 30); //****** needs to be adjusted 
  SDL_FreeSurface(optimized);
}

void Screen::displayPkmnCursor2() //pokemon 2
{
  Screen::displayPokemonMenu();
  SDL_Surface * myIm = IMG_Load("Cursor.png");
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 0, (float) 102); //****** needs to be adjusted 
  SDL_FreeSurface(optimized);
}

void Screen::displayPkmnCursor3() //pokemon 3
{
  Screen::displayPokemonMenu();
  SDL_Surface * myIm = IMG_Load("Cursor.png");
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 0, (float) 174); //****** needs to be adjusted 
  SDL_FreeSurface(optimized);
}

void Screen::displayPkmnCursor4() //pokemon 4
{
  Screen::displayPokemonMenu();
  SDL_Surface * myIm = IMG_Load("Cursor.png");
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 0, (float) 246); //****** needs to be adjusted 
  SDL_FreeSurface(optimized);
}

void Screen::displayPkmnCursor5() //pokemon 5
{
  Screen::displayPokemonMenu();
  SDL_Surface * myIm = IMG_Load("Cursor.png");
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 0, (float) 318); //****** needs to be adjusted 
  SDL_FreeSurface(optimized);
}

void Screen::displayPkmnCursor6() //pokemon 6
{
  Screen::displayPokemonMenu();
  SDL_Surface * myIm = IMG_Load("Cursor.png");
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 0, (float) 390); //****** needs to be adjusted 
  SDL_FreeSurface(optimized);
}

    
//Mutators
void Screen::updateHealth() //displays the health of each pokemon and updates if necessary 
{
  Screen::displayMenu();
  SDL_Surface * myIm = IMG_Load("BottomHealthbar.png"); //****** should be the swoopy bar
  SDL_Surface * optimized = SDL_DisplayFormat(myIm);
  SDL_FreeSurface(myIm);
  myGUI.DrawImage(optimized, (float) 220, (float) 200); //****** needs to be adjusted 
  SDL_FreeSurface(optimized);

  SDL_Surface * myIm1 = IMG_Load("TopHealthbar.png"); //****** should be the swoopy bar
  SDL_Surface * optimized1 = SDL_DisplayFormat(myIm1);
  SDL_FreeSurface(myIm1);
  myGUI.DrawImage(optimized1, (float) 0, (float) 20); //****** needs to be adjusted 
  SDL_FreeSurface(optimized1);


  std::vector<Pokemon> p1Active;
  std::vector<Pokemon> p2Active;
  int num_pokemon = 4; // This could be changed or could be a global variable!!!!!!!!!!!!!!!!!

  // look for the active pokemon
  std::vector<Pokemon> pokemons = player1.getPokemons();
  for(int i=0; i < num_pokemon; i++)
    {
      if((pokemons[i]).getActive())
	{
	p1Active.push_back(pokemons[i]);
	break;
	}
    }

  pokemons = player2.getPokemons();
  for(int i=0; i < num_pokemon; i++)
    {
      if((pokemons[i]).getActive())
	{
	p2Active.push_back(pokemons[i]);
	break;
	}
    }

  float totalHealth = (p1Active[0]).getTotalHealth();
  float health = (p1Active[0]).getHealth();
  float healthBar = health / totalHealth;
  float width = 147;

  myGUI.DrawRect((float) 290, (float) 245, (float) 150, (float) 30, 0xFFFFFF);

  std::stringstream sax;
  sax << (int) health << "/" << (int) totalHealth;
  std::string h1 = sax.str();
  myGUI.DrawText(h1, (float) 325, (float) 250); //**** Needs to be adjusted
 
  //player 1 pokemon health
  myGUI.DrawRect((float) 292, (float) 230, (healthBar * width) , (float) 6, 0x000000); //rectange x, y, width, height, ***fix coords

  totalHealth = (p2Active[0]).getTotalHealth();
  health = (p2Active[0]).getHealth();
  healthBar = health / totalHealth;

  myGUI.DrawRect((float) 93, (float) 77, (healthBar * width), (float) 6, (int) 0x000000); //***** fix coords

  std::stringstream sax1;
  sax1 << (int) health << "/" << (int) totalHealth;
  h1 = sax1.str(); // ***** Not sure that this works either
  myGUI.DrawText(h1, (float) 135, (float) 86); //**** Needs to be adjusted

  Screen::displayPokemon();
  Screen::displayPokemonNames();
  Screen::displayXP();

} 
