#include "Squirtle.h"

Squirtle::Squirtle(): name("squirtle"),  type("water")
{
	moves = new Move[3];
	moves[0] = Scratch();
	moves[1] = WaterGun();
	moves[2] = Shell();
}

~Squirtle()
{
	delete []moves;
}
