class Shock : Move
{

	public:
	Shock(): name("shock"), damage(25), type("electric"), cause_sleep(false), message("shocked") {}
	
	~Shock() {}

};
