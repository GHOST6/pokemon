#include <iostream>
#include <string>
#include <SDL/SDL.h> // same
#include <SDL/SDL_ttf.h> // might need to be SDL_TTF.h
#include <SDL/SDL_image.h> // same
#include "view.h"
#include "GUI.h"


using std::string;

int main()
{ 
    GUI tester;
    SDL_Surface * myIm = IMG_Load("TrainerScreen.png");
    if(myIm == NULL)
      {
	std::cerr << "omg it didn't work" << std::endl;
	exit(1);
      }
    SDL_Surface * optimized = SDL_DisplayFormat(myIm);
    SDL_FreeSurface(myIm);
    tester.DrawImage(optimized, (float) 0, (float) 0);
    tester.DrawRect((float) 18, (float) 310, (float) 400, (float) 100, 0xFFFFFF);
    TTF_Init();

    string test = "Naomi is going to get the worst peer evaluation in HISTORY :)";
    tester.DrawWrapText(test, (float) 20, (float) 310, 28);




    std::cout << "Tell me when you wanna quit by typing q" << std::endl;
    string q;
    std::cin >> q;
    
    TTF_Quit();
    SDL_Quit();

        
    return 0;
}
