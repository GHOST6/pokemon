class Tackle : Move
{

	public:
	Tackle(): Move(), name("tackle"), damage(10), type("none"), cause_sleep(false), message("used tackle on") {}

	~Tackle() {}
};
