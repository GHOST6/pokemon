#ifndef _MOVE_H
#define _MOVE_H

#include <string>

class Move
{
	protected:
	std::string name;
	std::string type;
	int damage;
	bool cause_sleep;
	std::string message;
	
	public:
	Move();
	~Move();
	std::string getName() const;
	std::string getType() const;
	int getDamage() const;
	bool getCause_sleep() const;
	std::string getMessage() const;
};
/*
class Ember : public Move
{
	public:
	Ember();
	~Ember();
};

class Flash : public Move
{
	public:
	Flash();
	~Flash();
};

class LeafBlade : public Move
{
	public:
	LeafBlade();
	~LeafBlade();
};

class Party : public Move
{
	public:
	Party();
	~Party();
};

class Scratch : public Move
{
	public:
	Scratch();
	~Scratch();
};

class Shell : public Move
{
	public:
	Shell();
	~Shell();
};

class Shock : public Move
{
	public:
	Shock();
	~Shock();
};

class SolarRegeneration : public Move
{
	public:
	SolarRegeneration();
	~SolarRegeneration();
};

class Tackle : public Move
{
	public:
	Tackle();
	~Tackle();
};

class WaterGun : public Move
{
	public:
	WaterGun();
	~WaterGun();
};
*/
#endif
