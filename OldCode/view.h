//
//  view.h
//  
//
//  Created by Steven Dalvin on 6/23/13.
//
//

#ifndef ____view__
#define ____view__

class view {

#include <iostream>
#include <string>
#include <SDL/SDL.h> // same
#include <SDL/SDL_ttf.h> // might need to be SDL_TTF.h
#include <SDL/SDL_image.h> // same

/** Draws the string of text starting at the (x,y) position.
 */
virtual void DrawText(std::string& text, float x, float y) = 0;

/** Draws the surface at the given location.
 */
virtual void DrawImage(SDL_Surface* image, float x, float y) = 0;

/** Draws a solid rectangle, for the health bar.
 */
 virtual void DrawRect(float x, float y, float width, float height, int color) = 0;
    
};


#endif /* defined(____view__) */
