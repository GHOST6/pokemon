class LeafBlade : Move
{

	public:
	LeafBlade(): name("leaf blade"),  damage(25), type("grass"), cause_sleep(false), message("used his razor sharp leaves to cut") {}

	~LeafBlade() {}

};
