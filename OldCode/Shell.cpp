class Shell : Move
{

	public:
	Shell(): name("shell"), damage(0), type("none"), cause_sleep(false), message("increased his defense by hiding in his shell from") {}

	~Shell() {}

	int use_move(Pokemon a, Pokemon b)
	{
		if (a.num_special <= 0)
			return 0;
		if ((--a.num_special) <= 0)
			delete a.moves[2];
		a.defense += 5;
		return 0;
	}

};
