//Steven Dalvin, 600.120, 6/21/13, assignment 6, 330-651-6324, sdalvin1, sdalvin1@jhu.edu

#ifndef ____Screen__
#define ____Screen__

#include <iostream>
#include <sstream>
#include <cstdlib>
#include "Trainer.h"
#include "Pokemon.h"
#include "Draw.h"
#include "GUI.h"
#include "View.h"

class Screen : public View
{
  Trainer player1;
  Trainer player2;
  GUI myGUI;
    
public:
  // Screen(); //default is white
    Screen(Trainer & p1, Trainer & p2); //takes two trainer classes
    
    //destructor
    ~Screen(); //destructor
    
    //Accessors
    //std::ostream & write(std::ostream &) const;
    void displayBackground(); //displays a white background with an empty box
    void displayTextBox(); //displays the blank text box
    void displayMenu(); //displays the menu
    void displayMoves(); //displays moves
    void displayItems(); //displays items menu
    void displayPokemon(); //displays the active pokemon
    void displayXP(); //displays the trainer's experience
    void displayPokemonNames(); //displays the names of the pokemon
    void displayPokemonMenu();
    void displayMenuCursor1(); //menu 1
    void displayMenuCursor2(); //menu 2
    void displayMenuCursor3(); //menu 3
    void displayMenuCursor4(); //menu 4
    void displayFightCursor1(); //move 1
    void displayFightCursor2(); //move 2
    void displayFightCursor3(); //move 3
    void displayFightCursor4(); //move 4
    void displayPkmnCursor1(); //pokemon 1
    void displayPkmnCursor2(); //pokemon 2
    void displayPkmnCursor3(); //pokemon 3
    void displayPkmnCursor4(); //pokemon 4
    void displayPkmnCursor5(); //pokemon 5
    void displayPkmnCursor6(); //pokemon 6

    
    //Mutators
    void updateHealth(); //displays the health of each pokemon and updates if necessary    
    
};



#endif /* defined(____Color__) */
