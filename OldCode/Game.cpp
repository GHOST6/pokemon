/*
Naomi Ephraim
Nick Blair
Steven Dalvin
Daniel Benarroch
Game.cpp
 */

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_mixer.h>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include "Trainer.h"
#include "Game.h"
#include "View.h"
#include "Model.h"
#include "GUI.h"
#include "Input.h"
#include "Music.h"

Game::Game() : done(false), cursor(0), t1(NULL), t2(NULL), endTurn(false), gui(NULL), input(NULL) { }

Game::Game(View * v, Controller * c, Trainer * trainer1, Trainer * trainer2) //constructor
{
  done = false;
  cursor = 0;
  gui = v;
  input = c;
  t1 = trainer1; //player 1
  t2 = trainer2; //player 2
  endTurn = false;
}

//destructor
Game::~Game() {}

Game & Game::operator=(const Game & g)
{
  done = g.done;
  cursor = g.cursor;
  gui = g.gui;
  input = g.input;
  t1 = g.t1;
  t2 = g.t2;
  endTurn = g.endTurn;
  return *this;
}

Pokemon * Game::getActivePokemon(Trainer * trainer)
{
  for (int i = 0; i < trainer->getNum_pokemon(); i++) { //find active pokemon
    if (trainer->getPokemon(i).getActive())
      return trainer->getPokemonPtr(i);
  }
  trainer->getPokemon(0).setActive(true); //else (this should never be reached)
  return trainer->getPokemonPtr(0);  
}

Trainer * Game::getCurrentTurn()
{
  if (t1->getIs_turn()) //checking whose turn
    return t1;
  return t2;
}

Trainer * Game::getOtherTrainer()
{
  if (t1->getIs_turn()) //finding whose turn
    return t2;
  return t1;
}

std::vector<Move> Game::getAbilities(Pokemon& pokemon)
{
  return pokemon.getMoves(); //return moves vector
}

int Game::getCursorPosition()  
{
  return cursor; //cursor position
}

void Game::drawMessage(std::string input, int wrapNum)
{
  gui->displayTextBox();
  gui->DrawWrapText(input, (float) 20, (float) 310, wrapNum); //call draw wrap text function
  gui->flip();
  SDL_Delay(2000); //delay
}

SDL_Surface * Game::loadImage(const char * filename)
{
  SDL_Surface * temp = IMG_Load(filename); //load an image
  if (temp)
    {
      SDL_Surface * optimized = SDL_DisplayFormat(temp); //set surface
      SDL_FreeSurface(temp); //free surface
      return optimized;
    }
  return 0;
}

void Game::switchTurn()
{
  endTurn = true; //flag to switch turns
  if (t1->getIs_turn()) { //switch
    t1->setIs_turn(false);
    t2->setIs_turn(true);
  }
  else { //switch
    t2->setIs_turn(false);
    t1->setIs_turn(true);
  }
}

int Game::gameLoop()
{
  if (SDL_Init(SDL_INIT_EVERYTHING) < 0)  { //initialize
    printf("Failed to initialize SDL\n");
    return 0;
  }
  atexit(SDL_Quit); //to do at exit
  if( TTF_Init() == -1 ) //initialize font
    return false;    

  Music m;
  m.startBattleMusic();

  SDL_WM_SetCaption("Pokemon", 0); //initialize window caption

  gui->displayBackground(); //display white background
  gui->flip();
  if (rand() % 2 == 1) //randomly choose who goes first
    t1->setIs_turn(true);
  else 
    t2->setIs_turn(true);
  std::cout<<getCurrentTurn()->getName()<<"\n";
  t1->setPokemonActive("pikachu", true);
  t2->setPokemonActive("pikachu", true);
  gui->displayPokemonMenu(); //display pokemon names
  gui->flip();
  cursor = 7; //set cursor
  while (cursor != 0) //before it returns to main menu
    doInput();
  std::cout<<getCurrentTurn()->getName()<<"\n";
  gui->displayPokemonMenu(); //display menu for second player, repeat
  gui->flip();
  cursor = 7;
  while (cursor!= 0)  
    doInput();
  unsigned previous = SDL_GetTicks();  
  unsigned framerate = 1000/60;  
  while (!done) { //while game is not over
    endTurn = false;
    gui->updateHealth(); //display current state
    gui->flip();
    std::stringstream stream; 
    stream << getCurrentTurn()->getName(); //get current turn
    stream << "\'s turn!";
    std::string input = stream.str();
    drawMessage(input, 16); //print whose turn it is
    //SDL_Delay(3000); //wait
    unsigned now = SDL_GetTicks();
    unsigned delta = now - previous;
    previous = now;
    if (delta < framerate)
      SDL_Delay(framerate - delta);
    std::cout << "got to here" << std::endl;
    while(!endTurn) //until the turn is over
      doInput();
  }
  m.stopBattleMusic();
  gui->freeVidScreen(); //free screen
  TTF_Quit(); //quit font
  SDL_Quit(); //quit sdl
  return true;
}

bool Game::beats(std::string a, std::string b)
{
  if ((a.compare("water") == 0) && (b.compare("fire") == 0))
    return true;
  else if ((a.compare("fire") == 0) && (b.compare("grass") == 0))
    return true;
  else if ((a.compare("grass") == 0) && (b.compare("electric") == 0))
    return true;
  else if ((a.compare("electric") == 0) && (b.compare("water") == 0))
    return true;
  return false;
}


std::string Game::use_move(Move m)
{
  Pokemon * a = getActivePokemon(getCurrentTurn()); //get active pokemon
  Pokemon * b = getActivePokemon(getOtherTrainer()); //get target pokemon
  std::stringstream stream;
  stream << a->getName() << " used " << m.getName();
  int miss = (rand() % 100);
  if (miss < 7) {
    stream << ". It missed!";
    std::string output = stream.str();
    return output;
  }
  if (m.getName().compare("flash") == 0) { //if flash is being used
    if(a->getNum_special() <= 0) //if out of special moves
      return "out of that move";
    if ((a->setNum_special(a->getNum_special() - 1)) <= 0)
      a->removeMove("flash"); //remove move if out of special
    b->setAttack(b->getAttack() - 5);
    stream << " decreasing the attack of " << b->getName() << " to " << b->getAttack() << ".";
    std::string output = stream.str(); //convert to string
    switchTurn(); //switch turn
    return output; //return message string
  }
  else if (m.getName().compare("party") == 0) { //if party is being used
    if (a->getNum_special() <= 0) //if out of special moves
      return "out of that move";
    if ((a->setNum_special(a->getNum_special() - 1)) <= 0)
      a->removeMove("party");
    int damage_done = a->getAttack() + m.getDamage() - b->getDefense(); //calculating damage done
    int crit = (rand() % 100); //if it's a critical hit
    if ((b->getName().compare("joanne") != 0) && ((b->getName().compare("james") == 0) || (crit < 10))) {
      damage_done += 7;
      stream << ", it was a critical hit!";
    }
    if (damage_done < 0)
      damage_done = 0;
    b->setHealth(b->getHealth() - damage_done);
    stream << b->getName() << " has a nasty hangover and is sleep.";
    if (b->getHealth() <= 0) { //checking if a pokemon has 0 health
      b->setHealth(0);
      std::string tempName = b->getName();
      stream << tempName << " fainted. ";
      getOtherTrainer()->removePokemon(b->getName());
      getOtherTrainer()->setNum_pokemon(getOtherTrainer()->getNum_pokemon() - 1);
      if (getOtherTrainer()->hasNoPokemon()) {
	stream << getOtherTrainer()->getName() << " has been defeated!";
	done = true;
      }
      else
        getOtherTrainer()->setActivePokemon(0, true);
    }
    std::string output = stream.str();
    switchTurn();
    switchTurn();
    return output;
  }
  else if (m.getName().compare("shell") == 0) { //if shell is being used
    if (a->getNum_special() <= 0) //if out of special moves
      return "out of that move";
    if ((a->setNum_special(a->getNum_special() - 1)) <= 0)
      a->removeMove("shell");
    a->setDefense(a->getDefense() + 5); //set defense
    stream << " increasing his defense to " << a->getDefense() << ".";
    std::string output = stream.str();
    switchTurn();
    return output;
  }
  else if (m.getName().compare("solar regeneration") == 0) { //if solar regeneration is being used
    if (a->getNum_special() <= 0) //if out of moves
      return "out of that move"; 
    if ((a->setNum_special(a->getNum_special() - 1)) <= 0)
      a->removeMove("solar regeration");
    a->setHealth(a->getHealth() + 8); //set health
    stream << " to regain 8 health.";
    std::string output = stream.str();
    switchTurn();
    return output;
  }
  else if (m.getName().compare("homework") == 0) {
    if (a->getNum_special() <= 0)
      return "out of theat move";
    if ((a->setNum_special(a->getNum_special() - 1)) <= 0) 
      a->removeMove("homework");
    int damage_done = a->getAttack() + m.getDamage() - b->getDefense();
    int crit = (rand() % 100); //if it's a critical hit
    if (crit < 10) {
      damage_done += 7;
      stream << ", it was a critical hit!";
    }
    if (damage_done < 0)
      damage_done = 0;
    b->setHealth(b->getHealth() - damage_done);
    stream << b->getName() << " is burried in a mountain of homework and is incapacitated for a turn.";
    if (b->getHealth() <= 0) { //checking if a pokemon has 0 health
      b->setHealth(0);
      std::string tempName = b->getName();
      stream << tempName << " fainted. ";
      getOtherTrainer()->removePokemon(b->getName());
      getOtherTrainer()->setNum_pokemon(getOtherTrainer()->getNum_pokemon() - 1);
      if (getOtherTrainer()->hasNoPokemon()) {
	stream << getOtherTrainer()->getName() << " has been defeated!";
	done = true;
      }
      else
        getOtherTrainer()->setActivePokemon(0, true);
    }
    std::string output = stream.str();
    switchTurn();
    switchTurn();
    return output;
  }
  else if (m.getName().compare("destruct") == 0) {
    done = true;
    stream << " which caused a massive SEGFAULT killing everyone and ending the game. NOTE: we meant to do this our code works, not a real segfault.";
    std::string output = stream.str();
    return output;
  }
  else { //any non-special move
    int damage_done = a->getAttack() + m.getDamage() - b->getDefense();
    if (beats(a->getType(), b->getType())) { //determines who wins based on type
      damage_done += 20;
      stream << ", it was super effective!";
      b->setHealth(b->getHealth() - damage_done); //set health
      std::string output = stream.str();
      switchTurn();
      return output;
    }
    else {
      int crit = (rand() % 100); //if it's a critical hit
      if (crit < 10) {
	damage_done += 7;
	stream << ", it was a critical hit!";
      }
    }
    if (damage_done < 0)
      damage_done = 0;
    b->setHealth(b->getHealth() - damage_done);
    if (b->getHealth() <= 0) { //checking if a pokemon has 0 health
      std::cout<<"setting health to 0 here \n";
      b->setHealth(0);
      std::cout<<b->getHealth()<<std::endl;
      std::string tempName = b->getName();
      stream << tempName << " fainted. ";
      getOtherTrainer()->removePokemon(b->getName());
      getOtherTrainer()->setNum_pokemon(getOtherTrainer()->getNum_pokemon() - 1);
      if (getOtherTrainer()->hasNoPokemon()) {
	stream << getOtherTrainer()->getName() << " has been defeated!";
	done = true;
      }
      else 
	getOtherTrainer()->setActivePokemon(0, true);
    }
    //put in dead animation
    std::string output = stream.str();
    switchTurn();
    return output;
  }
}



void Game::doInput()
{
  cursor = input->handleInput(cursor);
  std::cout << cursor << std::endl;
  switch(cursor) {
  case 0: //trainer screen, fight
    gui->displayMenu(); //display main menu
    gui->displayMenuCursor1(); //display cursor
    gui->flip();
    break;
  case 1: //trainer screen, item
    gui->displayMenuCursor2();
    gui->flip();
    break;
  case 2: //trainer screen, pkmn
    gui->displayMenuCursor3();
    gui->flip();
    break;
  case 3: //trainer screen, run
    gui->displayMenuCursor4();
    gui->flip();
    break;
  case 4: //fight screen basic
    gui->displayFightCursor1();
    gui->flip();
    break;
  case 5: //fight screen elemental
    gui->displayFightCursor2();
    gui->flip();
    break;
  case 6: //fight screen special
    gui->displayFightCursor3();
    gui->flip();
    break;
  case 7: //pkmn screen 1
    gui->displayPkmnCursor1();
    gui->flip();
    break;
  case 8: //pkmn screen 2
    gui->displayPkmnCursor2();
    gui->flip();
    break;
  case 9: //pkmn screen 3
    gui->displayPkmnCursor3();
    gui->flip();
    break;
  case 10: //pkmn screen 4
    gui->displayPkmnCursor4();
    gui->flip();
    break;
  case 11: //pkmn screen 5
    gui->displayPkmnCursor5();
    gui->flip();
    break;
  case 12: //pkmn screen 6
    gui->displayPkmnCursor6();
    gui->flip();
    break;
    //item cursors
  case 17: //pressing enter on trainer screen, moves
    gui->displayMoves(); //display moves menu
    gui->displayFightCursor1(); //display cursor
    gui->flip();
    cursor = 4;
    break;
  case 18: //pressing enter on trainer screen, pkmn
    gui->displayPokemonMenu(); //display pokemon menu
    gui->displayPkmnCursor1(); //display cursor
    gui->flip();
    cursor = 7;
    break;
  case 19: //pressing enter on trainer screen, item
    cursor = 13;
    gui->displayItems(); //display item menu
    gui->flip();
    //display cursor
    break;
  case 20: //pressing enter on move screen, basic
    {
      cursor = 0; //reset cursor
      gui->updateHealth(); //display current health 
      gui->flip();
      //display animation for basic move
      std::string input = use_move(getActivePokemon(getCurrentTurn())->getMove(0)); //do move
      gui->updateHealth(); //display current health
      gui->flip();
      drawMessage(input, 20); //output message
      break;
    }
  case 21: //pressing enter on move screen, elemental
    {
      cursor = 0;
      gui->updateHealth();
      gui->flip();
      //display animation for basic move  
      std::string input = use_move(getActivePokemon(getCurrentTurn())->getMove(1));
      gui->updateHealth();
      gui->flip();
      drawMessage(input, 20);
      break;
    }
  case 22: //pressing enter on move screen, special
    {
      if (getActivePokemon(getCurrentTurn())->getNum_special() <= 0) {
	cursor = 4;
	break;
      }
      cursor = 0;
      gui->updateHealth();
      gui->flip();
      //display animation for basic move  
      std::string input = use_move(getActivePokemon(getCurrentTurn())->getMove(2));
      gui->updateHealth();
      gui->flip();
      drawMessage(input, 20);
      break;
    }
  case 23: //pressing enter on pkmn screen, pokemon 1
    {
      cursor = 0; //reset cursor
      int numPokes = getCurrentTurn()->getPokemonsPtr()->size();
      for (int i = 0; i < numPokes; i++) {
	if (getCurrentTurn()->getPokemon(i).getActive())
	  getActivePokemon(getCurrentTurn())->setActive(false); //changing active pokemon
      }
      getCurrentTurn()->setPokemonActive(getCurrentTurn()->getPokemon(0).getName(), true); //setting active pokemon
      gui->displayPokemon(); //display pokemon
      gui->flip();
      std::stringstream stream;
      stream << getActivePokemon(getCurrentTurn())->getName();
      stream << " is ready to fight!";
      std::string input = stream.str();
      drawMessage(input, 20); //message
      switchTurn();
      break;
    }
  case 24: //pressing enter on pkmn screen, pokemon 2
    {
      cursor = 0;
      int numPokes = getCurrentTurn()->getPokemonsPtr()->size();
      for (int i = 0; i < numPokes; i++) {
	if (getCurrentTurn()->getPokemon(i).getActive())
	  getActivePokemon(getCurrentTurn())->setActive(false); //changing active pokemon
      }
      getCurrentTurn()->setPokemonActive(getCurrentTurn()->getPokemon(1).getName(), true);
      gui->displayPokemon();
      gui->flip();
      std::stringstream stream;
      stream << getActivePokemon(getCurrentTurn())->getName();
      stream << " is ready to fight!";
      std::string input = stream.str();
      drawMessage(input, 20);
      switchTurn();
      break;
    }
  case 25: //pressing enter on pkmn screen, pokemon 3
    {
      cursor = 0;
      int numPokes = getCurrentTurn()->getPokemonsPtr()->size();
      for (int i = 0; i < numPokes; i++) {
	if (getCurrentTurn()->getPokemon(i).getActive())
	  getActivePokemon(getCurrentTurn())->setActive(false); //changing active pokemon
      }
      getCurrentTurn()->setPokemonActive(getCurrentTurn()->getPokemon(2).getName(), true);
      gui->displayPokemon();
      gui->flip();
      std::stringstream stream;
      stream << getActivePokemon(getCurrentTurn())->getName();
      stream << " is ready to fight!";
      std::string input = stream.str();
      drawMessage(input, 20);
      switchTurn();
      break;
    }
  case 26: //pressing enter on pkmn screen, pokemon 4
    {
      cursor = 0;
      int numPokes = getCurrentTurn()->getPokemonsPtr()->size();
      for (int i = 0; i < numPokes; i++) {
	if (getCurrentTurn()->getPokemon(i).getActive())
	  getActivePokemon(getCurrentTurn())->setActive(false); //changing active pokemon
      }
      getCurrentTurn()->setPokemonActive(getCurrentTurn()->getPokemon(3).getName(), true);
      gui->displayPokemon();
      gui->flip();
      std::stringstream stream;
      stream << getActivePokemon(getCurrentTurn())->getName();
      stream << " is ready to fight!";
      std::string input = stream.str();
      drawMessage(input, 20);
      switchTurn();
      break;
    }
  case 27: //pressing enter on pkmn screen, pokemon 5
    {
      cursor = 0;
      int numPokes = getCurrentTurn()->getPokemonsPtr()->size();
      for (int i = 0; i < numPokes; i++) {
	if (getCurrentTurn()->getPokemon(i).getActive())
	  getActivePokemon(getCurrentTurn())->setActive(false); //changing active pokemon
      }
      getCurrentTurn()->setPokemonActive(getCurrentTurn()->getPokemon(4).getName(), true);
      gui->displayPokemon();
      gui->flip();
      std::stringstream stream;
      stream << getActivePokemon(getCurrentTurn())->getName();
      stream << " is ready to fight!";
      std::string input = stream.str();
      drawMessage(input, 20);
      switchTurn();
      break;
    }
  case 28: //pressing enter on pkmn screen, pokemon 6
    {
      cursor = 0;
      int numPokes = getCurrentTurn()->getPokemonsPtr()->size();
      for (int i = 0; i < numPokes; i++) {
	if (getCurrentTurn()->getPokemon(i).getActive())
	  getActivePokemon(getCurrentTurn())->setActive(false); //changing active pokemon
      }
      getCurrentTurn()->setPokemonActive(getCurrentTurn()->getPokemon(5).getName(), true);
      gui->displayPokemon();
      gui->flip();
      std::stringstream stream;
      stream << getActivePokemon(getCurrentTurn())->getName();
      stream << " is ready to fight!";
      std::string input = stream.str();
      drawMessage(input, 20);
      switchTurn();
      break;
    }
  case 29: //pressing enter on run, or escape in trainer screen
    cursor = 0;
    done = true;
    break;
  default: //pressing anything else
    break;
  }
}

int main(int argc, char * argv[])
{
  if (argc != 3) {
    std::cout << "Usage: Player names";
    return 0;
  }

  std::string name1(argv[1]);
  std::string name2(argv[2]);

  Trainer t1(name1);
  Trainer t2(name2);

  View * gui = new GUI(&t1, &t2);
  Controller * input = new Input();
  Model * game = new Game(gui, input, &t1, &t2);
  game->gameLoop();

  return 0;
}
