#include "sdbm.h"
#include <string>
#include <vector>
#include <cstdio>

std::vector<Trainer> trainers;
int entries=0; // the number of entries in playerData
std::string db = "database.db";

/** Peforms a bubble sort to order trainers by experience
 */
void bubbleSort()
{
  int swapped = 1;
  Trainer temp;
  if (entries <= 1) {
    return;
  }
  else {
    while (swapped == 1) {
      swapped = 0;
      for (int i = 1; i<entries; i++) {
	if (trainers[i-1].getExperience() < trainers[i].getExperience()) {
	  temp = trainers[i];
	  trainers[i] = trainers[i-1];
	  trainers[i-1] = temp;
	  swapped = 1;
	}
      }
    }
  }
}

void sdbm_create()
{
  FILE * file = fopen("database.db", "r");
  if (file)
    return;
  fclose(file);
  file = fopen("database.db", "w");
  fclose(file);
}

void sdbm_open()
{
  entries=0;
  FILE * file = fopen("database.db", "r");
  fseek(file, 0, SEEK_END);
  int place = ftell(file);
  rewind(file);
  entries = (int) place / (int) sizeof(Trainer);
  for (int i = 0; i < entries; i++) {
    Trainer t;
    t = fread(t, sizeof(Trainer), 1, file);
    trainers.push_back(t);
  }
  fclose(file);
}

void sdbm_sync()
{
  FILE * file = fopen("database.db", "w");
  bubbleSort();
  for (int i = 0; i < entries; i++) {
    fwrite(trainers[0], sizeof(Trainer), 1, file);
  }
  fclose(file);
}

void sdbm_close()
{
  sdbm_sync();
  delete [] trainers;
}

bool sdbm_has(std::string n) 
{
  for (int i = 0; i < entries; i++) {
    if(trainers[i].getName().compare(n)) == 0) {
      return true;
    }
  }
  return false;
}

void sdbm_insert(const Trainer * t)
{
  if (sdbm_has(t->getName())) {
    return;
  }
  trainers.push_back(*t);
  entries++;
}



