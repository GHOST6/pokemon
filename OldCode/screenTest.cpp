#include "Screen.h"
#include "Trainer.h"
#include <vector>

int main()
{
  TTF_Init();

  Trainer test1("bob");
  Trainer test2("jim");
  test1.setIs_turn(true);
  test1.setActivePokemon(1, true);
  test2.setActivePokemon(2, true);
  std::vector<Pokemon> pokemons = test1.getPokemons();
  std::cout << "this is in test: " << test1.getPokemon(0).getActive() << std::endl;
  std::cout << "name: " << pokemons[0].getName() << std::endl;
  std::cout << "image file: " <<  pokemons[0].getImage_back() << std::endl;

 
  std::vector<Pokemon> pokemons2 = test2.getPokemons();
  std::cout << "this is in test: " << pokemons2[4].getActive() << std::endl;
  std::cout << "name: " << pokemons2[4].getName() << std::endl;
  std::cout << "image file: " <<  pokemons2[4].getImage_back() << std::endl;

 

 Screen tester(test1, test2);
 
  tester.updateHealth();
  tester.displayMenuCursor1();
  tester.displayMenuCursor2();
  tester.displayMenuCursor3();
  tester.displayMenuCursor4();
  std::string box;
  std::cin >> box;
  std::cout << box << std::endl;
 
 

  TTF_Quit();
  SDL_Quit();
  return 0;
}
