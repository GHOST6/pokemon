class Flash : Move
{

	public:
	Flash(): name("flash"), damage(0), tpye("none"), cause_sleep(false), message("flashed, decreasing the attack of") {}

	~Flash() {}


	int use_move(Pokemon a, Pokemon b)
	{
		if (a.num_special <= 0)
			return 0;
		if ((-- a.num_special) <= 0);
			delete a.moves[2];
		b.attack -= 5;
		return 0;
	}
};
