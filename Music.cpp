/*
Naomi Ephraim – nephrai1@jhu.edu 571-435-6593
Steven Dalvin – sdalvin1@jhu.edu 330-651-6324
Nick Blair – nblair@jhu.edu 845-238-1555
Daniel Benarroch – dbenarr1@jhu.edu 410-960-3319
600.120
Music.cpp
6/28/13
 */

#include <iostream>
#include <cstdlib>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_mixer.h>
#include "Trainer.h"
#include "Pokemon.h"
#include "Music.h"

Music::Music()
{
openAudio();
music = Mix_LoadMUS("pokemonBattle.wav");
scratch = Mix_LoadWAV("scratch.wav");
ember = Mix_LoadWAV("ember.wav");
homework = Mix_LoadWAV("homework.wav");
tackle = Mix_LoadWAV("tackle.wav");
leafBlade = Mix_LoadWAV("leafBlade.wav");
java = Mix_LoadWAV("java.wav");
flash = Mix_LoadWAV("flash.wav");
party = Mix_LoadWAV("party.wav");
shock = Mix_LoadWAV("shock.wav");
waterGun = Mix_LoadWAV("waterGun.wav");
destruct = Mix_LoadWAV("destruct.wav");
shell = Mix_LoadWAV("shell.wav");
git = Mix_LoadWAV("git.wav");
solarRegeneration = Mix_LoadWAV("solarRegeneration.wav");
}

void Music::openAudio()
{
  if ( Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
    {
      std::cerr << "shit sumthins wrong in da mix" << std::endl;
    }
}

void Music::startBattleMusic()
{
  if (music == NULL)
    {
      std::cerr << "shit sumthins wrong with music" << std::endl;
    }
  
  Mix_VolumeMusic(60);
  Mix_PlayMusic(music, -1);
}

void Music::stopBattleMusic()
{
  Mix_HaltMusic();
  Mix_FreeMusic( music );
  Mix_CloseAudio();
}

void Music::scratchfx()
{
  if(scratch == NULL)
    {
      std::cerr << "something wrong with scratch" << std::endl;
    }
  Mix_PlayChannel(-1, scratch, 0);
}

void Music::freeScratchfx()
{
  Mix_FreeChunk(scratch);
}

void Music::emberfx()
{
  if(ember == NULL)
    {
      std::cerr << "something wrong with ember" << std::endl;
    }
  Mix_PlayChannel(-1, ember, 0);
}

void Music::freeEmberfx()
{
  Mix_FreeChunk(ember);
}

void Music::homeworkfx()
{
  if(homework == NULL)
    {
      std::cerr << "something wrong with homework" << std::endl;
    }
  Mix_PlayChannel(-1, homework, 0);
}

void Music::freeHomeworkfx()
{
  Mix_FreeChunk(homework);
}

void Music::tacklefx()
{
  if(tackle == NULL)
    {
      std::cerr << "something wrong with tackle" << std::endl;
    }
  Mix_PlayChannel(-1, tackle, 0);
}

void Music::freeTacklefx()
{
  Mix_FreeChunk(tackle);
}

void Music::leafBladefx()
{
  if(leafBlade == NULL)
    {
      std::cerr << "something wrong with ember" << std::endl;
    }
  Mix_PlayChannel(-1, ember, 0);
}

void Music::freeLeafBladefx()
{
  Mix_FreeChunk(leafBlade);
}

void Music::javafx()
{
  if(java == NULL)
    {
      std::cerr << "something wrong with java" << std::endl;
    }
  Mix_PlayChannel(-1, java, 0);
}

void Music::freeJavafx()
{
  Mix_FreeChunk(java);
}

void Music::flashfx()
{
  if(flash == NULL)
    {
      std::cerr << "something wrong with flash" << std::endl;
    }
  Mix_PlayChannel(-1, flash, 0);
}

void Music::freeFlashfx()
{
  Mix_FreeChunk(flash);
}

void Music::partyfx()
{
  if(party == NULL)
    {
      std::cerr << "something wrong with party" << std::endl;
    }
  Mix_PlayChannel(-1, party, 0);
}

void Music::freePartyfx()
{
  Mix_FreeChunk(party);
}

void Music::shockfx()
{
  if(shock == NULL)
    {
      std::cerr << "something wrong with shock" << std::endl;
    }
  Mix_PlayChannel(-1, shock, 0);
}

void Music::freeShockfx()
{
  Mix_FreeChunk(shock);
}

void Music::waterGunfx()
{
  if(waterGun == NULL)
    {
      std::cerr << "something wrong with waterGun" << std::endl;
    }
  Mix_PlayChannel(-1, waterGun, 0);
}

void Music::freeWaterGunfx()
{
  Mix_FreeChunk(waterGun);
}

void Music::destructfx()
{
  if(destruct == NULL)
    {
      std::cerr << "something wrong with destruct" << std::endl;
    }
  Mix_PlayChannel(-1, destruct, 0);
}
  void Music::freeDestructfx()
  {
    Mix_FreeChunk(destruct);
  }
void Music::shellfx()
{
  if(shell == NULL)
    {
      std::cerr << "something wrong with shell" << std::endl;
    }
  Mix_PlayChannel(-1, shell, 0);
}

void Music::freeShellfx()
{
  Mix_FreeChunk(shell);
}

void Music::gitfx()
{
  if(git == NULL)
    {
      std::cerr << "something wrong with git" << std::endl;
    }
  Mix_PlayChannel(-1, git, 0);
}

void Music::freeGitfx()
{
  Mix_FreeChunk(git);
}
void Music::solarRegenerationfx()
{
  if(solarRegeneration == NULL)
    {
      std::cerr << "something wrong with solarRegeneration" << std::endl;
    }
  Mix_PlayChannel(-1, solarRegeneration, 0);
}
void Music::freeSolarRegenerationfx()
{
  Mix_FreeChunk(solarRegeneration);
}

