/*
Naomi Ephraim – nephrai1@jhu.edu 571-435-6593
Steven Dalvin – sdalvin1@jhu.edu 330-651-6324
Nick Blair – nblair@jhu.edu 845-238-1555
Daniel Benarroch – dbenarr1@jhu.edu 410-960-3319
600.120
Draw.cpp
6/28/13
 */


 /* ----------------DOCUMENTATION-----------------
First initialize a GUI class instance

To display an image:
1. Load an image. 
 Ex:
     SDL_Surface * myIm = IMG_Load("TrainerScreen.png");

2. Optimize your image for SDL. Don't forget to free your original image. 
 Ex:
     SDL_Surface * optimized = SDL_DisplayFormat(myIm);
     SDL_FreeSurface(myIm);

To draw a rectangle:
1. Nothing special, just pass it the parameters it wants

To draw text:
1. In your code put: TTF_Init(); 
2. Make a string object to draw.
3. Call the function with the parameters it asks for
4. At the end of all your code, you must put TTF_Quit(); to shut down TTF. You should also put SDL_Quit() to shut down SDL
    ------------------------------------------------------ */
#include <iostream>
#include <string>
#include <SDL/SDL.h> // same
#include <SDL/SDL_ttf.h> // might need to be SDL_TTF.h
#include <SDL/SDL_image.h> // same
#include "Draw.h"
#include <sstream>

using std::string;

Draw::Draw() {}

SDL_Surface * screen = SDL_SetVideoMode(480, 432, 32, SDL_HWSURFACE);
  /*
Takes in a string, start position, and how many characters to write before it wraps and wraps the text you write around in a box
   */
void Draw::DrawWrapText(string& input, float x, float y, int wrapNum)
{
  float startSpot = x;
  float startSpot2 = y;
  int flag = 0;
  int charNum = wrapNum;
  string test = input;
  std::stringstream c;
  if((int) test.length() >= charNum)
    {
      string textString;
      int wrapTimes = (int) (test.length()) / charNum;
      for(int i=0; i < wrapTimes; i++)
	{
	  for(int m=0; m < charNum; m++)
	    {
	      if(m == 0 && std::isspace(test.c_str()[0 + (charNum * i)]))
		{
		  flag = 1;
		} 
	      if((unsigned int) (m + (charNum * i) + flag) != test.length())
		{
		  c << test.at(m + (charNum * i) + flag);
		  
		}
	      textString = c.str();
	      
	    }
	  
	  if(flag == 1)
	    {
	      textString[charNum - 1] = '\0';
	    }
	  
	  flag = 0;
	  Draw::DrawText(textString, startSpot, startSpot2);
	  startSpot2 += 20;
	  c.str(std::string());
	}
      int extraChars = (int) test.length() % charNum;
      int k;
      for(k=0; k < extraChars ; k++)
	{
	  textString[k] = (test.c_str())[k + (charNum * wrapTimes)];
	}
      if(extraChars != 0)
	{
	  textString[k] = '\0';
	  Draw::DrawText(textString, startSpot, startSpot2);
	}
    }
  else 
    Draw::DrawText(test, startSpot, startSpot2);
}

/** Draws the string of text starting at the (x,y) position.
 */
void Draw::DrawText(string& text, float x, float y) 
{
  TTF_Font * fontPtr = NULL; //font pointer for font structure
  
  fontPtr = TTF_OpenFont("PokeText", 15); //gets font pointer to text file for rendering the text
  if (fontPtr == NULL)
    {
      std::cerr << "font pointer failed." << std::endl;
      exit(1);
    }
  SDL_Color foreground = {0, 0, 0, 0}; // foreground color object
  SDL_Color background = {255, 255, 255, 0}; // background color object
  SDL_Surface * textSurface = TTF_RenderText_Shaded(fontPtr, text.c_str(), foreground, background);
  
  if (textSurface == NULL)
    {
      std::cerr << "text surface initialization failed" << std::endl;
      exit(1);
    }
  SDL_Rect textPosition = {(short) x, (short) y, 0, 0};
  
  Uint32 white = SDL_MapRGB(textSurface->format, 255, 255, 255);
  SDL_SetColorKey(textSurface, SDL_SRCCOLORKEY, white);
  
  SDL_BlitSurface(textSurface, NULL, screen, &textPosition); // blits the text to the screen
  
  SDL_FreeSurface(textSurface); //free's the text surface object
  TTF_CloseFont(fontPtr); //closes the font
  
  //***** THIS COULD BE IMPLEMENTED OUTSIDE THIS FUNCTION *********
  // SDL_Flip(screen); //updates the screen to show the text
  
  
  /*
    can clear the screen like this:
    SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0)); // will return 0 if it works
  */
  
}

/** Draws the surface at the given location. Assumes image is in the right format
 */
void Draw::DrawImage(SDL_Surface* image, float x, float y) 
{   
  if (image == NULL)
    {
      std::cerr << "SDL_LoadBMP() Failed" << std::endl;
      exit(1);
    }
  SDL_Rect position = {(short) x, (short) y, 0, 0};
  
  
  if(SDL_BlitSurface(image, NULL, screen, &position) != 0)
    {
      std::cerr << "SDL_BlitSurface() Failed" << std::endl;
      exit(1);
    }
  
  // SDL_Flip(screen); // updates display
  // I think this will just kill the screen: SDL_FreeSurface(imscreen);
}



/** Draws a solid rectangle, for the health bar. color should be in hex       */
void Draw::DrawRect(float x, float y, float width, float height, int color) 
{           
  SDL_Rect rectangle = {(short) x,(short) y,(unsigned short) width,(unsigned short) height};
  SDL_FillRect(screen, &rectangle, color);
  //	SDL_Flip(screen);
}

SDL_Surface * Draw::returnScreen()
{
  return screen;
}

void Draw::freeScreen()
{
  SDL_FreeSurface(screen);
}
   
