/*
Naomi Ephraim – nephrai1@jhu.edu 571-435-6593
Steven Dalvin – sdalvin1@jhu.edu 330-651-6324
Nick Blair – nblair@jhu.edu 845-238-1555
Daniel Benarroch – dbenarr1@jhu.edu 410-960-3319
600.120
View.h
6/28/13
 */


#ifndef _View_h_
#define _View_h_

#include <iostream>
#include <string>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_image.h>

class View {

 public:
  /** Displays a white background with an empty box.
   */
  virtual void displayBackground() = 0;
  
  /** Displays a text box. 
   */
  virtual void displayTextBox() = 0;

  /** Displays the top menu. 
   */
  virtual void displayMenu() = 0;
  
  /** Displays the available moves. 
   */
  virtual void displayMoves() = 0;

  /** Displays the available items. 
   */
  virtual void displayItems() = 0;

  /** Displays the active pokemon. 
   */
  virtual void displayPokemon() = 0;

  /** Displays the trainer's experience. 
   */
  virtual void displayXP() = 0;

  /** Displays the pokemon's names. 
   */
  virtual void displayPokemonNames() = 0;

  /** Displays the pokemon menu.
   */
  virtual void displayPokemonMenu() = 0;

  /** Displays the cursor at the first item in the menu. 
   */
  virtual void displayMenuCursor1() = 0;

  /** Displays the cursor at the second item in the menu. 
   */
  virtual void displayMenuCursor2() = 0;

  /** Displays the cursor at the third item in the menu. 
   */
  virtual void displayMenuCursor3() = 0;

  /** Displays the cursor at the fourth item in the menu. 
   */
  virtual void displayMenuCursor4() = 0;  

  /** Displays the cursor at the first item in the moves menu. 
   */
  virtual void displayFightCursor1() = 0;

  /** Displays the cursor at the second item in the moves menu. 
   */
  virtual void displayFightCursor2() = 0;

  /** Displays the cursor at the third item in the moves menu. 
   */
  virtual void displayFightCursor3() = 0;

  /** Displays the cursor at the fourth item in the moves menu. 
   */
  virtual void displayFightCursor4() = 0;

  /** Displays the cursor at the first item in the pokemon menu. 
   */
  virtual void displayPkmnCursor1() = 0;

  /** Displays the cursor at the second item in the pokemon menu. 
   */
  virtual void displayPkmnCursor2() = 0;

  /** Displays the cursor at the third item in the pokemon menu. 
   */
  virtual void displayPkmnCursor3() = 0;

  /** Displays the cursor at the fourth item in the pokemon menu. 
   */
  virtual void displayPkmnCursor4() = 0;

  /** Displays the cursor at the fifth item in the pokemon menu. 
   */
  virtual void displayPkmnCursor5() = 0;

  /** Displays the cursor at the sixth item in the pokemon menu. 
   */
  virtual void displayPkmnCursor6() = 0;

  /** Draws text and wraps after a certain number of characters. 
   */
  virtual void DrawWrapText(std::string&, float, float, int) = 0;

  /** Displays the health of each pokemon and updates if necessary. 
   */
  virtual void updateHealth() = 0;

  /** Frees the video screen
   */
  virtual void freeVidScreen() = 0;

  /** Flips the screen to display the new image. 
   */
  virtual void flip() = 0;

  /** Displays the start screen for the game
   */
  virtual void startScreen() = 0;
};

#endif
