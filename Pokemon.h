/*
Naomi Ephraim – nephrai1@jhu.edu 571-435-6593
Steven Dalvin – sdalvin1@jhu.edu 330-651-6324
Nick Blair – nblair@jhu.edu 845-238-1555
Daniel Benarroch – dbenarr1@jhu.edu 410-960-3319
600.120
Pokemon.h
6/28/13
 */

#ifndef _POKEMON_H
#define _POKEMON_H

#include "Move.h"
#include <vector>

class Pokemon
{
 protected:
  std::string name;
  std::string type;
  int health;
  int totalHealth;
  int attack;
  int defense;
  int num_special;
  bool active;
  std::string image_front;
  std::string image_back;
  std::string image_menu;
  int back_color;
  std::vector<Move>  moves;
  
  
 public:
  Pokemon(); //constructor
  ~Pokemon();  //destructor
  
  Pokemon(const Pokemon&); //copy constructor
  Pokemon & operator=(const Pokemon &); //equals operator
  friend std::ostream & operator<<(std::ostream &, const Pokemon &);
  
  //accesors
  /** Returns a pokemon's name. 
   */
  std::string getName() const { return name; }

  /** Returns a pokemon's type. 
   */
  std::string getType() const { return type; }

  /** Returns a pokemon's current health. 
   */
  int getHealth() const { return health; }

  /** Returns a pokemon's total health.  
   */
  int getTotalHealth() const { return totalHealth; }

  /** Returns a pokemon's attack strength. 
   */
  int getAttack() const { return attack; }

  /** Retuns a pokemon's defense strength. 
   */
  int getDefense() const { return defense; }

  /** Returns a pokemon's number of special moves. 
   */
  int getNum_special() const { return num_special; }

  /** Returns whether or not a pokemon is active. 
   */
  bool getActive() const { return active; }

  /** Returns the pokemon's front image. 
   */
  std::string getImage_front() const { return image_front; }

  /** Returns the pokemon's back image. 
   */
  std::string getImage_back() const { return image_back; }

  /** Returns the pokemon's image to print in the menu. 
   */
  std::string getImage_menu() const { return image_menu; }

  /** Returns the pokemon's back image color. 
   */
  int getBack_color() const { return back_color; }

  /** Returns a pokemon's moves vector.  
   */
  std::vector<Move> getMoves() const { return moves; }

  /** Returns a pokemon's move at index i. 
   */
  Move getMove(int i) const { return moves[i]; }
  
  
  //modifiers
  /** Sets a pokemon's health. 
   */
  void setHealth(int i) { health = i; }

  /** Sets a pokemon's attack. 
   */
  void setAttack(int i) { attack = i; }

  /** Sets a pokemon's defense. 
   */
  void setDefense(int i) { defense = i; }

  /** Sets a pokemon's active flag. 
   */
  void setActive(bool b) { active = b; }
  
  /** Sets a pokemon's number of special moves. 
   */
  int setNum_special(int i) { num_special = i; return num_special; }
  
  /** Removes a pokemon's move. 
   */
  void removeMove(std::string);
  //void removeMove(int i) { delete moves[i - 1]; }
};

class Bulbasaur : public Pokemon
{
 public:
  Bulbasaur();
  ~Bulbasaur();
  
};

class Charmander : public Pokemon
{
 public:
  Charmander();
  ~Charmander();
  
};

class Pikachu : public Pokemon
{
 public:
  Pikachu();
  ~Pikachu();  
  
};

class Squirtle : public Pokemon
{
 public:
  Squirtle();
  ~Squirtle();
  
};

class Joanne : public Pokemon
{
 public:
  Joanne();
  ~Joanne();
};

class James : public Pokemon
{
 public:
  James();
  ~James();
};
#endif
