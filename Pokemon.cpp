/*
Naomi Ephraim – nephrai1@jhu.edu 571-435-6593
Steven Dalvin – sdalvin1@jhu.edu 330-651-6324
Nick Blair – nblair@jhu.edu 845-238-1555
Daniel Benarroch – dbenarr1@jhu.edu 410-960-3319
600.120
Pokemon.cpp
6/28/13
 */

#include "Pokemon.h"

Pokemon::Pokemon(): health(100), totalHealth(100), attack(50), defense(50), num_special(3), active(false)  {}

Pokemon::Pokemon(const Pokemon & other)
{
  name = other.name;
  type = other.type;
  health = other.health;
  totalHealth = other.totalHealth;
  attack = other.attack;
  defense = other.defense;
  num_special = other.num_special;
  active = other.active;
  moves = other.moves;
  image_front = other.image_front;
  image_back = other.image_back;
  image_menu = other.image_menu;
  back_color = other.back_color;
}

Pokemon & Pokemon::operator=(const Pokemon & rhs)
{
  name = rhs.name;
  type = rhs.type;
  health = rhs.health;
  totalHealth = rhs.totalHealth;
  attack = rhs.attack;
  defense = rhs.defense;
  num_special = rhs.num_special;
  active = rhs.active;
  moves = rhs.moves;
  image_front = rhs.image_front;
  image_back = rhs.image_back;
  image_menu = rhs.image_menu;
  back_color = rhs.back_color;
  return * this;
}	

std::ostream & operator<<(std::ostream & os, const Pokemon & p)
{
  os << "==========\n" << p.getName();
  if (p.getActive() == true)
    os << " (currently battling)";
  os << "\n-----\ntype: " << p.getType() << "\n"
     << "health: " << p.getHealth() << "/" << p.getTotalHealth() << "\n"
     << "attack: " << p.getAttack() << "\n"
     << "defense: " << p.getDefense() << "\n"
     << "number of special moves remaining: " << p.getNum_special() << "\n"
     << "Moves:\n";
  for (int i = 0; i < 3; i++)
    os << p.moves[i] << "\n";
  os << "=====";
  return os;
}

void Pokemon::removeMove(std::string s)
{
  for (int i = 0; i < (int) moves.size(); i++)
    {
      if (moves[i].getName() == s)
	{
	  moves.erase(moves.begin() + i);
	  break;
	}
    }
}

Pokemon::~Pokemon() 
{
  moves.clear();
}


Bulbasaur::Bulbasaur(): Pokemon()
{
  name = "bulbasaur";
  type = "grass";
  health = 85;
  totalHealth = 85;
  image_front = "bulbasaur.png";
  image_back = "bulbasaurBack.png";
  image_menu = "bulbasaurMenu.png";
  back_color = 0x00FF00;
  moves.push_back(Tackle());
  moves.push_back(LeafBlade());
  moves.push_back(SolarRegeneration());
}
Bulbasaur::~Bulbasaur() { }



Charmander::Charmander(): Pokemon()
{
  name = "charmander";
  type = "fire";
  attack = 60;
  defense = 40;
  image_front = "charmander.png";
  image_back = "charmanderBack.png";
  image_menu = "charmanderMenu.png";
  back_color = 0xFF3333;
  moves.push_back(Tackle());
  moves.push_back(Ember());
  moves.push_back(Flash());
}
Charmander::~Charmander() { }


Pikachu::Pikachu(): Pokemon()
{
  name = "pikachu";
  type = "electric";
  health = 80;
  totalHealth = 80;
  attack = 55;
  defense = 55;
  image_front = "pikachu.png";
  image_back = "pikachuBack.png";
  image_menu = "pikachuMenu.png";
  back_color = 0xFFFF00;
  moves.push_back(Scratch());
  moves.push_back(Shock());
  moves.push_back(Party());
}
Pikachu::~Pikachu() { }


Squirtle::Squirtle(): Pokemon()
{
  name = "squirtle";
  type = "water";
  image_front = "squirtle.png";
  image_back = "squirtleBack.png";
  image_menu = "squirtleMenu.png";
  back_color = 0x00FFFF;
  moves.push_back(Scratch());
  moves.push_back( WaterGun());
  moves.push_back(Shell());
}
Squirtle::~Squirtle() { }


Joanne::Joanne(): Pokemon()
{
  name = "joanne";
  type = "professor";
  image_front = "joanne.png";
  image_back = "joanneBack.png";
  image_menu = "joanneMenu.png";
  back_color = 0xFF66FF;
  moves.push_back(Scratch());
  moves.push_back(Java());
  moves.push_back(Homework());
}
Joanne::~Joanne() { }

James::James(): Pokemon()
{
  name = "james";
  image_front = "james.png";
  image_back = "jamesBack.png";
  image_menu = "jamesMenu.png";
  back_color = 0xFF9900;
  moves.push_back(Tackle());
  moves.push_back(Git());
  moves.push_back(Destruct());
}
James::~James() { }
