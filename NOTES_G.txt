POKEMON GAME:

– pokemons(basic, elem, spec): squirtle(tackle, water gun, shell+2/+6), charmander(scratch, ember, flash-3max), bulbasaur(tackle, vine whip, Solar Regen- +6+9+12), pikachu(scratch, thundershock, Party-3max)

– CLASSES: Move class, pokemon class, Game class, trainer class, moveType class -inherit from move-, specPokemon class -inherited from pokemon-,

MOVE CLASS: (make a subclass for each move type)

Move.h
Move.cpp :

string name
int damage

SUBCLASS
MoveType class


GAME:

Game.h
Game.cpp



POKEMON:

Pokemon.h

vars:
string name;
int health;
int defense;
int numSpecial;
float experienceCoeff;
bool awake;




funct/constr:

Pokemon(string);
Pokemon(pokemon); COPY CONSTR


TRAINER:

Trainer.h
Trainer.cpp





//for health increase of bulbasaur 1-e^{initialHealth}


SPECIFICS:
Let us have four trainers each of us and we fight one battle each.
(could we do playing from different computers?)



IMPLEMENTATIONS:

– flags for which pokemon is on and for whose turn it is and iterate through them.
– we have 