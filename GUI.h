/*
Naomi Ephraim – nephrai1@jhu.edu 571-435-6593
Steven Dalvin – sdalvin1@jhu.edu 330-651-6324
Nick Blair – nblair@jhu.edu 845-238-1555
Daniel Benarroch – dbenarr1@jhu.edu 410-960-3319
600.120
GUI.h
6/28/13
*/

#ifndef _GUI_H_
#define _GUI_H_

#include <iostream>
#include <sstream>
#include <cstdlib>
#include "Trainer.h"
#include "Pokemon.h"
#include "Draw.h"
#include "View.h"

class GUI : public View
{
  Trainer *player1;
  Trainer *player2;
  Draw myGUI;
    
public:
  // Screen(); //default is white
  GUI(Trainer * p1, Trainer * p2); //takes two trainer classes
  
  //destructor
  ~GUI(); //destructor
  
  //Accessors
  //std::ostream & write(std::ostream &) const;
  void displayBackground(); //displays a white background with an empty box
  void displayTextBox(); //displays the blank text box
  void displayMenu(); //displays the menu
  void displayMoves(); //displays moves
  void displayItems(); //displays items menu
  void displayPokemon(); //displays the active pokemon
  void displayXP(); //displays the trainer's experience
  void displayPokemonNames(); //displays the names of the pokemon
  void displayPokemonMenu();
  void displayMenuCursor1(); //menu 1
  void displayMenuCursor2(); //menu 2
  void displayMenuCursor3(); //menu 3
  void displayMenuCursor4(); //menu 4
  void displayFightCursor1(); //move 1
  void displayFightCursor2(); //move 2
  void displayFightCursor3(); //move 3
  void displayFightCursor4(); //move 4
  void displayPkmnCursor1(); //pokemon 1
  void displayPkmnCursor2(); //pokemon 2
  void displayPkmnCursor3(); //pokemon 3
  void displayPkmnCursor4(); //pokemon 4
  void displayPkmnCursor5(); //pokemon 5
  void displayPkmnCursor6(); //pokemon 6
  void DrawWrapText(std::string&, float, float, int);
  
  //Mutators
  void updateHealth(); //displays the health of each pokemon and updates if necessary   
  void startScreen();
  void freeVidScreen();
  void flip();
  
};



#endif /* defined(____Color__) */
