/*
Naomi Ephraim – nephrai1@jhu.edu 571-435-6593
Steven Dalvin – sdalvin1@jhu.edu 330-651-6324
Nick Blair – nblair@jhu.edu 845-238-1555
Daniel Benarroch – dbenarr1@jhu.edu 410-960-3319
600.120
Controller.h
6/28/13
 */


#ifndef _CONTROLLER_H
#define _CONTROLLER_H

class Controller
{
 public:
  enum MenuItem
  {
    FIGHT, //0
    PKMN,
    ITEM,
    RUN,
    BASIC, //4
    ELEMENT,
    SPECIAL,
    P1, //7
    P2,
    P3,
    P4,
    P5,
    P6,
    I1, //13
    I2,
    I3,
    I4,
    ENTERFIGHT, //17
    ENTERPKMN,
    ENTERITEM,
    MOVE1, //20
    MOVE2,
    MOVE3,
    PKMN1, //23
    PKMN2,
    PKMN3,
    PKMN4,
    PKMN5,
    PKMN6,
    DONE //29
  };

  /* Handles keyboard input. 
   */
  virtual int handleInput(int) = 0;

} ;

#endif
