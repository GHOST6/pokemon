
=============
GENERAL INFO:
=============

TEAM SQRT(-11) // CS 600.120 // Assignment 7 – POKEMON GAME // 06/28/2013 //

========
MEMBERS:
========

Naomi Ephraim – nephrai1@jhu.edu 571-435-6593
Steven Dalvin – sdalvin1@jhu.edu 330-651-6324
Nick Blair – nblair@jhu.edu 845-238-1555 
Daniel Benarroch – dbenarr1@jhu.edu 410-960-3319

================
IMPORTANT NOTES:
================

1 – DO NOT HOVER THE MOUSE ON TOP OF THE DISPLAY/GAME SCREEN
2 - EXIT GAME THROUGH ESCAPE KEY AT MAIN MENU OR THROUGH RUN

=========
COMMENTS:
=========

Throughout this assignment we were able to learn about the different aspects of team work by experiencing the difficulties such a project entails. Although we had some disagreements at times, we always respected each other and each other's views and ideas. At the beginning, when the specifics of the game were not decided and we only had the global picture, we had a few more stressful moments in trying to understand and figure out how all the interfaces had to be implemented and how the logic of the game would proceed. Once we had most of the interfaces implemented (which was around Tuesday) we were able to step back and picture the connection between interfaces and apply these to the global picture of the game. 

We started by having different tasks given to the different members of the teams such as Input, GUI, Game and Trainer/Pokemon/Move classes, but little by little we saw how each of us started helping in other classes too, some by curiosity and others because it was needed. 

In general, we liked and enjoyed creating the game, not only because Pokemon was part of our childhood (for most of us), but also because it was the first game creating experience, at least for most of us.

In total, we spent over 190 hours on this project. 

==================
FILES AND CONTENT:
==================

MakePokemon – makes executable called game by calling "make -f MakePokemon"

Model.h – Game interface
Game.h – Game header file/class definition (includes View.h, Controller.h, Screen.h, Trainer.h)
Game.cpp – Our main function is here and the game logic implementation.

Controller.h – Input interface 
Input.h – Input header file/class definition
Input.cpp – Input implementation, handling the keyboard events and returning the desired action to be done by the cursor.

Draw.h – Draw header file/class definition
Draw.cpp – Draw implementation, functions which will allow for the display of the game.

View.h – GUI interface
GUI.h – GUI header file/class definition 
GUI.cpp – Implements GUI specific functions that make it easier to display the game, move cursor, display pokemon, display menus and messages.

Trainer.h – Trainer header file/class definition. Also includes Pokemon.h
Trainer.cpp – Implements all the trainer functions to get information on the situation of the fight.

Pokemon.h – Pokemon header file/class definition. Contains class definitions for all pokemon and each inherits from Pokemon class. Also includes Move.h
Pokemon.cpp – Implements pokemon functions to get information about the pokemon such as health, name and type.

Move.h – Move header file/class definition. Contains class definitions for all different moves and each inherits from Move class.
Move.cpp – Implements all moves and move functions to get information about each move such as damage, critical or not and sleep effect or not. 



*.png – all picture files.

*.wav – all music/sound effect files.

