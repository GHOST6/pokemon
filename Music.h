/*
Naomi Ephraim – nephrai1@jhu.edu 571-435-6593
Steven Dalvin – sdalvin1@jhu.edu 330-651-6324
Nick Blair – nblair@jhu.edu 845-238-1555
Daniel Benarroch – dbenarr1@jhu.edu 410-960-3319
600.120
Music.h
6/28/13
 */

#ifndef ___Music__
#define ___Music__

#include <SDL/SDL_mixer.h>
#include <iostream>
#include <cstdlib>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_image.h>
#include "Trainer.h"
#include "Pokemon.h"

class Music {


  //private variables, one per move
  Mix_Music * music;
  Mix_Chunk * scratch;
  Mix_Chunk * ember;
  Mix_Chunk * homework;
  Mix_Chunk * tackle;
  Mix_Chunk * leafBlade;
  Mix_Chunk * java;
  Mix_Chunk * flash;
  Mix_Chunk * party;
  Mix_Chunk * shock;
  Mix_Chunk * waterGun;
  Mix_Chunk * destruct;
  Mix_Chunk * git;
  Mix_Chunk * shell;
  Mix_Chunk * solarRegeneration;

 public:
  Music(); //constructor

  /** Opens the audio. 
   */
  void openAudio();

  /** Starts the game music. 
   */
  void startBattleMusic();

  /** Ends the game music. 
   */
  void stopBattleMusic();

  /** Plays the scratch sound effect. 
   */
  void scratchfx();

  /** Frees the scratch sound effect. 
   */
  void freeScratchfx();

  /** Plays the ember sound effect. 
   */
  void emberfx();

  /** Frees the ember sound effect. 
   */
  void freeEmberfx();

  /** Plays the homework sound effect. 
   */
  void homeworkfx();

  /** Frees the homework sound effect. 
   */
  void freeHomeworkfx();

  /** Plays the tackle sound effect. 
   */
  void tacklefx();

  /** Frees the tackle sound effect. 
   */
  void freeTacklefx();

  /** Plays the leaf blade sound effect. 
   */
  void leafBladefx();

  /** Frees the leaf blade sound effect. 
   */
  void freeLeafBladefx();

  /** Plays the java sound effect. 
   */
  void javafx();

  /** Frees the java sound effect. 
   */
  void freeJavafx();

  /** Plays the flash sound effect. 
   */
  void flashfx();

  /** Frees the flash sound effect. 
   */
  void freeFlashfx();

  /** Plays the party sound effect. 
   */
  void partyfx();

  /** Frees the party sound effect. 
   */
  void freePartyfx();

  /** Plays the shock sound effect. 
   */
  void shockfx();

  /** Frees the shock sound effect. 
   */
  void freeShockfx();

  /** Plays the water gun sound effect. 
   */
  void waterGunfx();

  /** Frees the water gun sound effect. 
   */
  void freeWaterGunfx();

  /** Plays the destruct sound effect. 
   */
  void destructfx();

  /** Frees the destruct sound effect. 
   */
  void freeDestructfx();

  /** Plays the git sound effect. 
   */
  void gitfx();

  /** Frees the git sound effect. 
   */
  void freeGitfx();

  /** Plays the shell sound effect. 
   */
  void shellfx();

  /** Frees the shell sound effect. 
   */
  void freeShellfx();

  /** Plays the solar regeneration sound effect. 
   */
  void solarRegenerationfx();

  /** Frees the solar regeneration sound effect. 
   */
  void freeSolarRegenerationfx();



};
#endif
