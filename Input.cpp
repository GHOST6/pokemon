/*
Naomi Ephraim – nephrai1@jhu.edu 571-435-6593
Steven Dalvin – sdalvin1@jhu.edu 330-651-6324
Nick Blair – nblair@jhu.edu 845-238-1555
Daniel Benarroch – dbenarr1@jhu.edu 410-960-3319
600.120
Input.cpp
6/28/13
 */


#include "Input.h"
#include <iostream>

Input::Input() {
	
}

Input::~Input() {}

int Input::handleInput(int cursor)
{   
  SDL_Event event; //create an event
  int pollEvent = SDL_PollEvent(&event);
  
  Uint8 *keyState = SDL_GetKeyState(NULL);
  int key = 0;
  if(pollEvent){
    SDL_WaitEvent(&event);  //wait for the event
    switch (event.type) { //switch on which event
    case SDL_QUIT: //if quit
      cursor = 29;
      return cursor;
    case SDL_KEYUP:
      switch (event.key.keysym.sym)
      default:
	return cursor;	
    case SDL_KEYDOWN: //if a keyboard button is pressed
      switch (event.key.keysym.sym) {
      case SDLK_UP:
	if (keyState[SDLK_UP]){
	  key = 1;}
	break;
      case SDLK_DOWN:
	if (keyState[SDLK_DOWN]){
	  key = 2;}
	break;
      case SDLK_RETURN:
	if (keyState[SDLK_RETURN]){
	  key = 3;}
	break;
      case SDLK_ESCAPE:
	if (keyState[SDLK_ESCAPE]){
	  key = 4;}
	break;
      case SDLK_LEFT:
	if (keyState[SDLK_LEFT]){
	  key = 5;}
	break;
      case SDLK_RIGHT:
	if (keyState[SDLK_RIGHT]){
	  key = 6;}
	break;
      default:
	break; 
      }
      
      if (cursor >= 0 && cursor <= 3) { //if the game is at the trainer screen
	switch (key) {
	case 1: case 2:
	  if (cursor == 0 || cursor == 1) { //will move cursor up or down
	    cursor = (cursor + 1) % 2;
	    return cursor;
	  }
	  if (cursor == 2) {
	    cursor = 3;
	    return cursor;
	  }
	  if (cursor == 3) {
	    cursor = 2;
	    return cursor;
	  }
	case 5: case 6: //will move cursor right or left
	  if (cursor == 0) {
	    cursor = 2;
	    return cursor;
	  }
	  if (cursor == 2) {
	    cursor = 0;
	    return cursor;
	  }
	  if (cursor == 1) {  
	    cursor = 3;
	    return cursor;
	  }	
	  if (cursor == 3) {
	    cursor = 1;
	    return cursor;
	  }
	case 3: //if enter is pressed
	  switch (cursor) {
	  case 0: 
	    cursor = 17; //tells game to enter fight menu
	    return cursor;
	  case 1:
	    cursor = 19; //tells game to enter pkmn menu
	    return cursor;
	  case 2:
	    cursor = 18; //tells game to enter item menu
	    return cursor;
	  case 3:
	    cursor = 29; //enter run menu, quit
	    return cursor;
	  default:
	    break;
	  }
	  return cursor;
	case 4: //quit
	  cursor = 29;
	  return cursor;
	default: //any other key
	  return cursor;
	}
      }
      if (cursor >=4 && cursor <= 6) { //if were at the fight box 
	switch (key) { //switch on key pressed
	case 2:
	  cursor = ((cursor - 3) % 3) + 4; //move cursor down
	  return cursor;
	case 1:
	  cursor = ((cursor - 2) % 3) + 4; //move cursor up
	  return cursor;
	case 3: //choose a move
	  cursor += 16;	
	  return cursor;
	case 4: //return to main menu
	  cursor = 0;
	  return cursor;
	default: //other
	  return cursor;
	}
      }
      if (cursor >= 7 && cursor <=12) { //pkmnbox //FOR SIX: change 10 to 12
	switch (key) {
	case 2:
	  cursor = (cursor - 6) % 6 + 7; // for six pkmn change to mod 6
	  return cursor;
	case 1:
	  cursor = (cursor + 4) % 6 + 7; // for six pkmn change to mod 6
	  return cursor;
	case 3: //choose a pokemon
	  cursor += 16;
	  return cursor;
	case 4: //return to main menu
	  cursor = 0;
	  return cursor;
	default:
	  return cursor;
	}
      }
      if (cursor >= 13 && cursor <= 16) {
	//space for item implimentation 
      }
    }
  }
  return cursor;
}
