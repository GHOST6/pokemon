/*
Naomi Ephraim – nephrai1@jhu.edu 571-435-6593
Steven Dalvin – sdalvin1@jhu.edu 330-651-6324
Nick Blair – nblair@jhu.edu 845-238-1555
Daniel Benarroch – dbenarr1@jhu.edu 410-960-3319
600.120
Move.cpp
6/28/13
 */

#include <iostream>
#include <string>
#include <list>
#include <cstdlib>
#include "Move.h"


Move::Move() : name(""), type(""), damage(0), message("") { }

Move::Move(const Move & other)
{
  name = other.name;
  type = other.type;
  damage = other.damage;
  message = other.message;
}

Move & Move::operator=(const Move & rhs)
{
  name = rhs.name;
  type = rhs.type;
  damage = rhs.damage;
  message = rhs.message;
  return * this;
}

std::ostream & operator<<(std::ostream & os, const Move & m)
{
  os << "----------\n" <<m.getName() <<"\n.....\n"
     << "type: " << m.getType() << "\n"
     << "damage: " << m.getDamage()
     << "\n-----";
  return os;
}

Move::~Move() { }


Destruct::Destruct(): Move()
{
  name = "destruct";
  type = "none";
  damage = 100;
  message = "self destructed killing ";
}
Destruct::~Destruct() { }


Ember::Ember(): Move()
{
  name = "ember";
  type = "fire";
  damage = 25;
  message = "used Ember to scorch ";
}
Ember::~Ember() { }


Flash::Flash()
{
  name = "flash";
  damage = 0;
  type = "none";
  message = "flashed, decreasing the attack of ";
}
Flash::~Flash(){ }


Git::Git(): Move()
{
  name = "git";
  damage = 40;
  message = "used Git to commit, overwriting ";
}
Git::~Git() { }


Homework::Homework(): Move()
{
  name = "homework";
  damage = 40;
  message = "used homework to burry ";
}
Homework::~Homework() { }


Java::Java(): Move()
{
  name = "java";
  damage = 40;
  message = "used her massive knowledge of Java to overwhelm ";
}
Java::~Java() { }


LeafBlade::LeafBlade()
{
  name = "leaf blade";
  damage = 25;
  type = "grass";
  message = "used his razor sharp leaves to cut";
}
LeafBlade::~LeafBlade(){ }


Party::Party()
{
  name = "party";
  damage = 5;
  type = "none";
  message = "threw an awesome party, giving a hangover to";
}
Party::~Party(){ }


Scratch::Scratch()
{
  name = "scratch";
  damage = 10;
  type = "none";
  message = "used scratch on";
}
Scratch::~Scratch(){ }


Shell::Shell()
{
  name = "shell";
  damage = 0;
  type = "none";
  message = "increased his defense by hiding in his shell from";
}
Shell::~Shell(){ }


Shock::Shock()
{
  name = "shock";
  damage = 25;
  type = "electric";
  message = "shocked";
}
Shock::~Shock(){ }


SolarRegeneration::SolarRegeneration()
{
  name = "solar regeneration";
  damage = 0;
  type = "none";
  message = "regenerated some health and didn't attack";
}
SolarRegeneration::~SolarRegeneration() { }

Tackle::Tackle()
{
  name = "tackle";
  damage = 10;
  type = "none";
  message = "used tackle on";
}
Tackle::~Tackle(){ }


WaterGun::WaterGun()
{
  name = "water gun";
  damage = 25;
  type = "water";
  message = "used water guns to blast";
}
WaterGun::~WaterGun(){ }
