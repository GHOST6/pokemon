/*
Naomi Ephraim – nephrai1@jhu.edu 571-435-6593
Steven Dalvin – sdalvin1@jhu.edu 330-651-6324
Nick Blair – nblair@jhu.edu 845-238-1555
Daniel Benarroch – dbenarr1@jhu.edu 410-960-3319
600.120
Input.h
6/28/13
*/

#include <iostream>
#include <stdlib.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include "Controller.h"
#include <SDL/SDL.h>

class Input : public Controller
{
 public:
  Input();
  ~Input();
  int handleInput(int);
};
