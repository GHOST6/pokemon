/*
Naomi Ephraim – nephrai1@jhu.edu 571-435-6593
Steven Dalvin – sdalvin1@jhu.edu 330-651-6324
Nick Blair – nblair@jhu.edu 845-238-1555
Daniel Benarroch – dbenarr1@jhu.edu 410-960-3319
600.120
Move.h
6/28/13
 */

#include <iostream>
#include <string>
#include <list>

#ifndef _MOVE1_H
#define _MOVE1_H

class Move
{
  //protected members
  protected:
  std::string name;
  std::string type;
  int damage;
  std::string message;

 public:
  Move(); // default constructor
  Move(const Move&); // copy constructor
  Move & operator=(const Move &); //assignment operator
  friend std::ostream & operator<<(std::ostream &, const Move &);
  ~Move(); //destructor 

  //accessors
  /** Returns the name of a move. 
   */
  std::string getName() const { return name; }

  /** Returns the type of a move. 
   */
  std::string getType() const { return type; }

  /** Returns the move's damage. 
   */
  int getDamage() const { return damage; }

  /** Returns the message that the move outputs. 
   */
  std::string getMessage() const { return message; }

};

class Destruct : public Move
{
 public:
  Destruct();
  ~Destruct();
};

class Ember : public Move
{
 public:
  Ember();
  ~Ember();
  
};

class Flash : public Move
{
 public:
  Flash();
  ~Flash();
};

class Git : public Move
{
 public:
  Git();
  ~Git();
};

class Homework : public Move
{
 public:
  Homework();
  ~Homework();
};

class Java : public Move
{
 public:
  Java();
  ~Java();
};

class LeafBlade : public Move
{
 public:
  LeafBlade();
  ~LeafBlade();
};

class Party : public Move
{
 public:
  Party();
  ~Party();
};

class Scratch : public Move
{
 public:
  Scratch();
  ~Scratch();
};

class Shell : public Move
{
 public:
  Shell();
  ~Shell();
};

class Shock : public Move
{
 public:
  Shock();
  ~Shock();
};

class SolarRegeneration : public Move
{
 public:
  SolarRegeneration();
  ~SolarRegeneration();
};

class Tackle : public Move
{
 public:
  Tackle();
  ~Tackle();
};

class WaterGun : public Move
{
 public:
  WaterGun();
  ~WaterGun();
};
#endif 
