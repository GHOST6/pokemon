/*
Naomi Ephraim – nephrai1@jhu.edu 571-435-6593
Steven Dalvin – sdalvin1@jhu.edu 330-651-6324
Nick Blair – nblair@jhu.edu 845-238-1555
Daniel Benarroch – dbenarr1@jhu.edu 410-960-3319
600.120
Draw.h
6/28/13
*/

 /* ----------------DOCUMENTATION-----------------
First initialize a GUI class instance

To display an image:
1. Load an image. 
 Ex:
     SDL_Surface * myIm = IMG_Load("TrainerScreen.png");

2. Optimize your image for SDL. Don't forget to free your original image. 
 Ex:
     SDL_Surface * optimized = SDL_DisplayFormat(myIm);
     SDL_FreeSurface(myIm);

To draw a rectangle:
1. Nothing special, just pass it the parameters it wants

To draw text:
1. In your code put: TTF_Init(); 
2. Make a string object to draw.
3. Call the function with the parameters it asks for
4. At the end of all your code, you must put TTF_Quit(); to shut down TTF. You should also put SDL_Quit() to shut down SDL
    ------------------------------------------------------ */
#ifndef _Draw_H_
#define _Draw_H_

#include <iostream>
#include <string>
#include <SDL/SDL.h> // same
#include <SDL/SDL_ttf.h> // might need to be SDL_TTF.h
#include <SDL/SDL_image.h> // same

class Draw 
{
public:
  Draw();

  /*
    Takes in a string, start position, and how many characters to write before it wraps and wraps the text you write around in a box
  */
  void DrawWrapText(std::string& input, float x, float y, int wrapNum);
  
  /** Draws the string of text starting at the (x,y) position.
   */
  void DrawText(std::string& text, float x, float y);
  
  /** Draws the surface at the given location. Assumes image is in the right format
   */
  void DrawImage(SDL_Surface* image, float x, float y); 
  
  /** Draws a solid rectangle, for the health bar.        */
  void DrawRect(float x, float y, float width, float height, int color);
  
  SDL_Surface * returnScreen();
  
  void freeScreen();
  
};
#endif      
