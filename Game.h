/*
Naomi Ephraim – nephrai1@jhu.edu 571-435-6593
Steven Dalvin – sdalvin1@jhu.edu 330-651-6324
Nick Blair – nblair@jhu.edu 845-238-1555
Daniel Benarroch – dbenarr1@jhu.edu 410-960-3319
600.120
Game.h
6/28/13
 */

#ifndef _GAME_H
#define _GAME_H

#include <vector>
#include <string>
#include "Controller.h"
#include "Move.h"
#include "Pokemon.h"
#include "View.h"
#include "Model.h"

class Game : public Model
{
  bool done;
  int cursor;
  Trainer * t1;
  Trainer * t2;
  bool endTurn;
  View * gui;
  Controller * input;

 public:
  //default constructor
  Game();

  //constructor
  Game(View*, Controller*, Trainer *, Trainer *);


  //destructor
  ~Game();

  Game & operator=(const Game &);

  /** Returns a trainer's active pokemon. 
   */
  Pokemon *  getActivePokemon(Trainer *);

  /** Returns the trainer who has the current turn. 
   */
  Trainer *  getCurrentTurn();

  /** Returns the trainer who has the next turn. 
   */
  Trainer * getOtherTrainer();

  /** Returns the vector of moves for a particular pokemon. 
   */
  std::vector<Move> getAbilities(Pokemon &);

  /** Returns the current cursor position.  
   */
  int getCursorPosition();

  /** Prints the current message. 
   */
  void drawMessage(std::string);

  /** Loads an image. 
   */
  SDL_Surface * loadImage(const char *);

  /** Switches whose turn it is. 
   */
  void switchTurn();

  /** Plays the game. 
   */
  int gameLoop();

  /** Determines which types of moves beat others. 
   */
  bool beats(std::string, std::string);

  /** Does a pokemon move. 
   */
  std::string use_move(Move);

  /** Calls all functions related to input. 
   */
  void doInput();
};

#endif
