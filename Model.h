/*
Naomi Ephraim – nephrai1@jhu.edu 571-435-6593
Steven Dalvin – sdalvin1@jhu.edu 330-651-6324
Nick Blair – nblair@jhu.edu 845-238-1555
Daniel Benarroch – dbenarr1@jhu.edu 410-960-3319
600.120
Model.h
6/28/13
 */

#ifndef _MODEL_H
#define _MODEL_H

#include "Move.h"
#include <string>
#include "Pokemon.h"
#include "Trainer.h"
#include <SDL/SDL.h>
#include <vector>

class Model
{
 public:
  /** Gets the active Pokemon for a given trainer.
   */
  virtual Pokemon * getActivePokemon(Trainer *) = 0;

  /** Gets the active trainer.
   */
  virtual Trainer * getCurrentTurn() = 0;

  /** Gets the unactive trainer. 
   */
  virtual Trainer * getOtherTrainer() = 0;

  /** Gets a vector of the abilities for a given Pokemon.
   */
  virtual std::vector<Move> getAbilities(Pokemon& pokemon) = 0;
        
  /** Gets the current cursor position.
   */
  virtual int getCursorPosition() = 0;

  /** Output the current message. 
   */
  virtual void drawMessage(std::string) = 0;

  /** Load an image onto the screen. 
   */
  virtual SDL_Surface * loadImage(const char * ) = 0;

  /** Switches whose turn it is. 
   */
  virtual void switchTurn() = 0;

  /** Loop to play the game. 
   */
  virtual int gameLoop() = 0;

  /** Use a move. 
   */
  virtual std::string use_move(Move) = 0;

  /** Determines which types of moves beat others.  
   */
  virtual bool beats(std::string, std::string) = 0;

  /** Do actions based on input. 
   */
  virtual void doInput() = 0;
};

#endif
