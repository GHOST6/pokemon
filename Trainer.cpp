/*
Naomi Ephraim – nephrai1@jhu.edu 571-435-6593
Steven Dalvin – sdalvin1@jhu.edu 330-651-6324
Nick Blair – nblair@jhu.edu 845-238-1555
Daniel Benarroch – dbenarr1@jhu.edu 410-960-3319
600.120
Trainer.cpp
6/28/13
 */

#include "Trainer.h"
Trainer::Trainer(): name(""), experience(0), num_pokemon(6), is_turn(false) { }

Trainer::Trainer(const Trainer & other)
{
  name = other.name;
  experience = other.experience;
  num_pokemon = other.num_pokemon;
  pokemons = other.pokemons;
  is_turn = other.is_turn;
}

Trainer & Trainer::operator=(const Trainer & rhs)
{
  name = rhs.name;
  experience = rhs.experience;
  num_pokemon = rhs.num_pokemon;
  pokemons = rhs.pokemons;
  is_turn = rhs.is_turn;
  return * this;
}

std::ostream & operator<<(std::ostream & os, Trainer & t)
{
  os << "//////////\n" << t.getName();
  if (t.getIs_turn() == true)
    os << " (currently making move)";
  os << "\n=====\n"
     << "experience: " << t.getExperience() << "\n"
     << "number of Pokemon: " << t.getNum_pokemon() << "\n"
     << "Pokemon:\n";
  for (int i = 0; i < (int) t.getPokemons().size(); i++)
    os << t.getPokemon(i) << "\n";
  os << "/////";
  return os; 
  
}

Trainer::Trainer(std::string n): name(n), experience(0), num_pokemon(6), is_turn(false)
{
  pokemons.push_back(Squirtle());
  pokemons.push_back(Charmander());
  pokemons.push_back(Bulbasaur());
  pokemons.push_back(Pikachu());
  pokemons.push_back(Joanne());
  pokemons.push_back(James());
}

void Trainer::removePokemon(std::string s)
{
  for (int i = 0; i < (int) pokemons.size(); i++)
    {
      if (pokemons[i].getName() == s)
	{
	  pokemons.erase(pokemons.begin() + i);
	  break;
	}
    }
}

void Trainer::setPokemonHealth(std::string s, int health)
{
  for (int i = 0; i < (int) pokemons.size(); i++)
    {
      if (pokemons[i].getName() == s)
	{
	  pokemons[i].setHealth(health);
	  break;
	}
    }
}

void Trainer::setPokemonAttack(std::string s, int attack)
{
  for (int i = 0; i < (int) pokemons.size(); i++)
    {
      if (pokemons[i].getName() == s)
	{
	  pokemons[i].setAttack(attack);
	  break;
	}
    }
}

void Trainer::setPokemonDefense(std::string s, int defense)
{
  for (int i = 0; i < (int) pokemons.size(); i++)
    {
      if (pokemons[i].getName() == s)
	{
	  pokemons[i].setHealth(defense);
	  break;
	}
    }
}

void Trainer::setPokemonActive(std::string s, bool b)
{
  for (int i = 0; i < (int) pokemons.size(); i++)
    {
      if (pokemons[i].getName() == s)
	{
	  pokemons[i].setActive(b);
	  break;
	}
    }
}


Trainer::~Trainer() { }
